;;;; This file is part of Aspiwork Project Data Management Server
;;;; Name:          permission.lisp
;;;; Purpose:       
;;;; Programmer:    Aashish R Lokhande
;;;; Copyright (C) 2010,2011 Aashish R Lokhande
;;;; All rights reserved.
;;;;
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions
;;;; are met:
;;;; 1. Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 2. Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;; 3. Neither the name of the author nor the names of its contributors
;;;;    may be used to endorse or promote products derived from this software
;;;;    without specific prior written permission.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
;;;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
;;;; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
;;;; OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;;;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
;;;; LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
;;;; OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
;;;; SUCH DAMAGE.

(defcomponent permission-page (holder)
  ((desig-id :accessor desig-id :initarg :desig-id :backtrack t
	    :initform 0))
  (:default-initargs :message "Designation Permission Editor"))

(defmethod render :wrapping ((self permission-page))
  (call-next-method)
  (let ((desid (desig-id self)))
    (<:p (<:ah (desig-name-from-id desid)))
    (<:hr)
    (<:ah "List of Permissions")
    (render (make-instance 'show-permissions
			   :desig-id desid))  
    (<:hr)
    (<:ah "Add new Permission")
    (render (make-instance 'get-permission
			   :desig-id desid))))

(defcomponent show-permissions () 
  ((desig-id :accessor desig-id :initarg :desig-id :backtrack t
	    :initform 0)))

(defmethod render ((self show-permissions))
  (let ((user $user)
	(desid (desig-id self))
	(catid 0))    
    (<ucw:form 
     :action (remove-permission desid catid)
    (<:table 
      :border 1 :style "margin:auto;"
      (<:tr (<:td "Allowed")(<:td "Select"))
      (loop for (id a) in (permission-list desid)
	 do (<:tr (<:td (<:ah a))
		  (<:td (<ucw:input :type "radio" :name "selected"
				    :accessor catid :value id)))))
    ;(<:input :type "submit" :value "Remove Category")
    )))

(defaction remove-permission (desid catid)
  ())

(defcomponent get-permission () 
  ((desig-id :accessor desig-id :initarg :desig-id :backtrack t
	    :initform 0)))

(defmethod render ((self get-permission))
  (let ((user $user)
	(desid (desig-id self))
	(catid 0))    
    (<ucw:form 
     :action (add-permission desid catid)
     (<:table 
      :style "margin:auto;"
      (<:tr 
       (<:td :align "center"
	     (<ucw:select :accessor catid
			  (<ucw:option :value 0 "Category")
			  (loop for (id b) in (res-cat-ddlist)
			     do (<ucw:option :value id (<:ah b))))
	     (<:ah " ")
	     (<:input :type "submit" :value "Add Category")))))))

(defaction add-permission (desid catid)
  (when (plusp catid)
    (unless (car (clsql:select [id] :from [permissions]
			  :where [and
			  [= [desig_id] desid] [= [category_id] catid] ]
			  :flatp t))
    (clsql:insert-records
     :into [permissions]
     :av-pairs
     `((desig_id ,(qp-to-int desid))
       (category_id ,(qp-to-int catid)))))))

(defun permission-list (des-id)
  (clsql:select
   [slot-value 'permission 'id]
   [slot-value 'resource-category 'name]
   :from '([permissions][resource_categories])
   :where
   [and
   [=[slot-value 'permission 'desig-id] des-id]
   [=[slot-value 'permission 'category-id]
   [slot-value 'resource-category 'id]]
   ]))
