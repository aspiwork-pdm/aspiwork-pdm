;;;; This file is part of Aspiwork Project Data Management server
;;;; Name:          company.lisp
;;;; Purpose:       
;;;; Programmer:    Aashish R Lokhande
;;;; Copyright (C) 2010,2011 Aashish R Lokhande
;;;; All rights reserved.
;;;;
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions
;;;; are met:
;;;; 1. Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 2. Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;; 3. Neither the name of the author nor the names of its contributors
;;;;    may be used to endorse or promote products derived from this software
;;;;    without specific prior written permission.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
;;;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
;;;; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
;;;; OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;;;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
;;;; LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
;;;; OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
;;;; SUCH DAMAGE.

(defcomponent company-manipulator ()
  ((company :accessor company :initarg :company :backtrack t
            :initform (make-instance 'company))))

(defcomponent show-company (company-manipulator)
  ())

(defmethod render ((self show-company))
  (let ((tcompany (company self)))
    (<:p (<:ah "Basic Company Information"))
    (in-table
     ((<:label "Company Name") (<:ah (name tcompany)))
     ((<:label "Company Short Name") (<:ah (extension tcompany)))
     ((<:label "Filestore Folder") (<:ah (folder tcompany)))
     ((<:label "Filestore IP") (<:ah (localip tcompany)))
     ((<:label "Filestore Username") (<:ah (fsuser tcompany))))    
    (render (make-instance 'show-address 
			   :address (address-from-id (address-id tcompany))))))

(defcomponent get-company (company-manipulator holder)
  ()
  (:default-initargs :message "Company Editor"))

(defmethod render :wrapping ((self get-company))
  (call-next-method)
  (let* (;(user $user)
	 (tcompany (company self))
	 (taddress (if (slot-boundp tcompany 'id)
		       (address tcompany)
		       (make-instance 'address))))
    (<ucw:form :action (update-company self tcompany taddress) 
	       :method "post"
	       (<:p (<:ah "Basic Company Information"))
	       (in-table
		((<:label "Code")
		 (<ucw:input :type "text" :accessor (code tcompany)))
		((<:label "Name")
		 (<ucw:input :type "text" :accessor (name tcompany)))
		((<:label "Short Name")
		 (<ucw:input :type "text" :accessor (extension tcompany)))		
		((<:label "Filestore IP")
		 (<ucw:input :type "text" :accessor (localip tcompany)))		
		((<:label "Filestore Username")
		 (<ucw:input :type "text" :accessor (fsuser tcompany))))
	       (<:p (<:ah "Address"))
	       (render (make-instance 'get-address :address taddress))
	       (<:table 
		:style "margin:auto;"
		(<:tr 
		 (<:td :align "center"
		       (<:input :type "submit" :value "Submit" )))))))

(defaction update-company ((self get-company) (tcompany company) (taddress address))
  (let* ((co-errors (validate-company tcompany))
	 (ad-errors (validate-address taddress))
	 (all-errors (concatenate 'string co-errors ad-errors)))
	(if (string= "" all-errors)
		(progn
		  (insert-update-address taddress)
		  (if (slot-boundp tcompany 'id)
		      (progn
			;(setf (modifier-id tcompany) (id user))
			(setf (modified-date tcompany) (clsql:get-time))
			(insert-update-company tcompany))
		      (progn
			(setf (slot-value tcompany 'id) 1)
			(setf (slot-value tcompany 'address-id) (id taddress))
			(setf (slot-value tcompany 'folder) 
			      (create-company-folder (fsuser tcompany)))
			(setf (created-date tcompany) (clsql:get-time))
			(clsql:update-records-from-instance tcompany)
			(create-admin tcompany)))
		  (answer))
		(setf (message self) 
		      (concatenate 'string "Errors in: " input-errors)))))

(defgeneric validate-company (company)
  (:documentation "check for essential fields and input errors"))

(defmethod validate-company ((company company))
  (let ((input-errors ""))
    (progn
      (setf (slot-value company 'name) (sanitize-input (name company)))
      (setf (slot-value company 'extension) 
	    (string-downcase (sanitize-input (extension company))))           
      (if (string= "" (name company))
	  (concatenate 'string input-errors
		       "Name "))      
      (if (or (string= "" (extension company))
	      (> 15 (length (extension company)))
	      (position-if-not #'alpha-char-p (extension company)))
	  (concatenate 'string input-errors
		       "Short-name ")))
    input-errors))

(defaction edit-company (id)
  (call-component $holder (make-instance
			   'get-company :company (company-from-id 1))))

(defun create-company-folder (fsuser)
  (let ((filestore
	 (concatenate 'string "/home/" fsuser "/projects/")))
    (ensure-directories-exist filestore)
    filestore))

(defun create-admin (company)
  (let ((admin 
	  (make-instance 'employee
			 :person-id 0						 
			 :dept-id 0
			 :desig-id 0
			 :code "ADMN"
			 :username "admin"
			 :password (extension company)
			 :manager-id 0)))
    (insert-update-employee admin)))
