;;;; This file is part of Aspiwork Project Data Management Server
;;;; Name:          load-pdm.lisp
;;;; Purpose:       
;;;; Programmer:    Aashish R Lokhande
;;;; Copyright (C) 2010,2011 Aashish R Lokhande
;;;; All rights reserved.
;;;;
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions
;;;; are met:
;;;; 1. Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 2. Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;; 3. Neither the name of the author nor the names of its contributors
;;;;    may be used to endorse or promote products derived from this software
;;;;    without specific prior written permission.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
;;;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
;;;; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
;;;; OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;;;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
;;;; LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
;;;; OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
;;;; SUCH DAMAGE.

(defcomponent person-manipulator ()
  ((person :accessor person :initarg :person :backtrack t
            :initform (make-instance 'person))))

(defcomponent show-person (person-manipulator)
  ())

(defmethod render ((self show-person))
  (let ((tperson (person self)))	
    (in-table
     ((<:label "Title") (<:ah (title tperson)))
     ((<:label "First Name") (<:ah (first-name tperson)))
     ((<:label "Middle Name") (<:ah (middle-name tperson)))
     ((<:label "Last Name") (<:ah (last-name tperson)))
     ((<:label "Gender") (<:ah (gender tperson)))
     ((<:label "Birth Date") (<:ah (birth-date tperson)))
     ((<:label "Married") (<:ah (married tperson))))))

(defcomponent get-person (person-manipulator)
  ())

(defmethod render ((self get-person))
  (let* ((tperson (person self))	 
	 (bdate (birth-date tperson)))
    (multiple-value-bind (bd bm by)(clsql:decode-date bdate)
      (in-table
       ((<:label "Title")      
	(<ucw:select :accessor (title tperson)
		     (<ucw:option :value "Mr" "Mr")
		     (<ucw:option :value "Mrs" "Mrs")
		     (<ucw:option :value "Ms" "Ms")
		     (<ucw:option :value "Dr" "Dr")))
       ((<:label "First Name")
	(<ucw:input :type "text" :accessor (first-name tperson)))
       ((<:label "Middle Name")
	(<ucw:input :type "text" :accessor (middle-name tperson)))
       ((<:label "Last Name")
	(<ucw:input :type "text" :accessor (last-name tperson)))
       ((<:label "Gender")
	"M:"(<ucw:input :type "radio" :name "gender"
			:accessor (gender tperson) 
			:value "M")
	" F:"(<ucw:input :type "radio" :name "gender"
			 :accessor (gender tperson) 
			 :value "F"))
       ((<:label "Birth Date")
	"D :" (<ucw:select :accessor bd
			   (dotimes (x 31) 
			     (<ucw:option 
			      :value (+ 1 x) (<:ah (+ 1 x)))))
	" M :" (<ucw:select :accessor bm
			    (dotimes (x 12) 
			      (<ucw:option 
			       :value (+ 1 x) (<:ah (+ 1 x)))))
	" Y :" (<ucw:select :accessor by
			    (do ((x 1950 (1+ x))) ((>= x 2020)) 
			      (<ucw:option :value x (<:ah x))))
	(<ucw:input :type "hidden"
		    :writer (lambda (val)
			      (declare (ignore val))
			      (setf (slot-value tperson 'birth-date)
				    (clsql:make-date 
				     :day bd
				     :month bm
				     :year by)))))
       ((<:label "Married")
	(<ucw:input :type "checkbox" 
		    :accessor (married tperson) :value 1
		    :checked (if (= 1 (married tperson)) t)))))))

(defgeneric validate-person (person)(:documentation ""))

(defmethod validate-person ((person person))
  (let ((input-errors))
    (progn
      (setf (slot-value person 'married) (qp-to-int (married person)))
      (setf (slot-value person 'first-name)(sanitize-input (first-name person)))
      (setf (slot-value person 'middle-name)(sanitize-input (middle-name person)))
      (setf (slot-value person 'last-name)(sanitize-input (last-name person)))
      (if (string= "" (first-name person)) 
	  (setf input-errors "First/Last Name ")))
    input-errors))

(defun next-person-id ()
  (if (= 0 (car (clsql:select [count [*]] :from [persons] :flatp t)))
      1
      (+ 1 (car 
	    (clsql:select [max [id]] 
			  :from [persons]
			  :flatp t)))))

(defun insert-update-person (obj)
  (if (slot-boundp obj 'id)
      (clsql:update-records-from-instance obj)
      (clsql:with-transaction ()	  
		(setf (id obj) (next-person-id))
		(clsql:update-records-from-instance obj))))

(defun person-from-id (per-id) ;object
  (car (clsql:select 'person 
		:where [=[slot-value 'person 'id] per-id]
		:flatp t)))

(defun person-name-from-id (per-id);name string
  (car (clsql:select
	[concat
	[slot-value 'person 'first-name] " "
	[slot-value 'person 'middle-name] " "
	[slot-value 'person 'last-name]]
	:from [persons]
	:where [=[slot-value 'person 'id] per-id]
	:field-names nil
	:flatp t)))
