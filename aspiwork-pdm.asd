;;;; This file is part of Aspiwork Project Data Management System
;;;; Name:          aspiwork-pdm.asd
;;;; Purpose:       
;;;; Programmer:    Aashish R Lokhande
;;;; Copyright (C) 2010,2011 Aashish R Lokhande
;;;; All rights reserved.
;;;;
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions
;;;; are met:
;;;; 1. Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 2. Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;; 3. Neither the name of the author nor the names of its contributors
;;;;    may be used to endorse or promote products derived from this software
;;;;    without specific prior written permission.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
;;;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
;;;; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
;;;; OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;;;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
;;;; LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
;;;; OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
;;;; SUCH DAMAGE.

(defpackage :aspiwork-pdm.system
  (:use :cl :asdf))

(in-package :aspiwork-pdm.system)

(defsystem aspiwork-pdm
  :description "Aspiwork Product/Project Data Management Server"
  :author "Aashish Lokhande <aashish.atllp@gmail.com>"
  :version "0.4.3"
  :serial t
  :depends-on (:ucw )
  :components ((:file "package")
	       (:file "pdm-server")
	       (:file "db")
	       (:file "helper")
	       (:file "main")
	       (:file "login")
	       (:file "user-dashboard")
	       (:file "address")
	       (:file "company")
	       (:file "department")
	       (:file "designation")
	       (:file "permission")
	       (:file "person")
	       (:file "employee")
	       (:file "resource-category")
	       (:file "project")
	       (:file "project-dashboard")
	       (:file "resource")
	       (:file "customer")
	       (:file "agency")))
