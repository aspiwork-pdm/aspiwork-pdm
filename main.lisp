;;;; This file is part of Aspiwork Project Data Management Server
;;;; Name:          main.lisp
;;;; Purpose:       defines window and menu components
;;;; Programmer:    Aashish R Lokhande
;;;; Copyright (C) 2010,2011 Aashish R Lokhande
;;;; All rights reserved.
;;;;
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions
;;;; are met:
;;;; 1. Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 2. Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;; 3. Neither the name of the author nor the names of its contributors
;;;;    may be used to endorse or promote products derived from this software
;;;;    without specific prior written permission.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
;;;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
;;;; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
;;;; OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;;;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
;;;; LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
;;;; OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
;;;; SUCH DAMAGE.

(defclass aspiwork-component ()
  ((user :accessor user
         :type (or employee null)
         :initarg :user )))

(defcomponent aspiwork-window (standard-window-component)
  ()
  (:default-initargs 
      :body (make-instance 'aspiwork-base-component)
    :title "Aspiwork Product Data Management Server"))

(defcomponent aspiwork-base-component (aspiwork-component)
  ((links :accessor links :initarg :links :component links)	 
   (holder :accessor holder :initarg :holder :component holder)))

(define-symbol-macro $window (context.window-component *context*))
(define-symbol-macro $body (window-body $window))
(define-symbol-macro $holder (holder $body))
(define-symbol-macro $links (links $body))
(define-symbol-macro $user (user $body))

(defcomponent links ()())

(defmethod render ((self links))
  (<:ah "Links"))

(defcomponent holder ()
  ((message :initform "Message" :accessor message :initarg :message)))

(defmethod render ((self holder))
  (<:h5 (<:as-html (message self))))

(defaction logout ()
  (mark-session-expired (context.session *context*))
  (call-as-window 'redirect-component
		  :target "login"))

(defaction contact-admin ()
  )

(defmethod render ((self aspiwork-base-component))  
  (<:table 
   :style "border:1px solid black; margin:0 auto; width:800px;"
   (<:tr (<:td :align "left" (<:ah (get-session-value 'user)))
	 (<:td :align "right" (<ucw:a :action (logout) "Logout"))))
  (<:hr)
  (<:div :style "border:1px solid black; margin:0 auto; width:800px;"
	 (render (links self))) 
  (<:hr)
  (<:div :style "border:1px solid black; margin:0 auto; width:800px;"
	 (render (holder self)))
  (<:hr)
  (<:table 
   :style "border:1px solid black; margin:0 auto; width:800px;"
   (<:tr (<:td :align "center" "Aspiwork Product Data Management Server"))
   (<:tr (<:td :align "center" (<ucw:a :action (contact-admin) "Contact Administrator")))))

(defentry-point "dashboard" (:application *aspiwork-pdm-application*)
  ()
  (if (get-session-value 'user)
      (let* ((user (get-user (get-session-value 'user))))
	(if (and user (not (disabled user)))
	    (cond
	      ((string= "admin" (username user))
	       (call 
		(make-instance 'aspiwork-window
			       :body (make-instance 
				      'aspiwork-base-component 
				      :user user
				      :links (make-instance 'company-links)
				      :holder (make-instance 
					       'show-company
					       :company (car (clsql:select 'company
		     :where [= [slot-value 'company 'id] 1]
		     :flatp t
		     :refresh t)))))))
	      (t
	       (call
		(make-instance 'aspiwork-window
			       :body (make-instance 
				      'aspiwork-base-component
				      :user user
				      :links (make-instance 'user-links)
				      :holder (make-instance 'user-summary))))))
	    (call 'aspiwork-login)))
      (call 'aspiwork-login)))

(defcomponent company-links (links)
  ())

(defmethod render ((self company-links)) 
  ;(call-next-method)
  (<:table 
   :border 1 :style "margin:auto;"
   (<:tr 
    (<:td (<ucw:a 
	   :action (call-component 
		    $holder (make-instance 'show-company))
	   "Summary"))
    (<:td (<ucw:a 
	   :action (call-component 
		    $holder (make-instance 'departments-page))
	   "Departments"))
    (<:td (<ucw:a 
	   :action (call-component 
		    $holder (make-instance 'designations-page))
	   "Designations"))
    (<:td (<ucw:a 
	   :action (call-component 
		    $holder (make-instance 'res-cat-page))
	   "Resource-Types"))
    (<:td (<ucw:a 
	   :action (call-component 
		    $holder (make-instance 'list-employees))
	   "Employees"))    
    (<:td (<ucw:a 
	   :action (call-component 
		    $holder (make-instance 'list-projects))
	   "Projects"))
    (<:td (<ucw:a 
	   :action (call-component 
		    $holder (make-instance 'list-customers))
	   "Customers"))
    (<:td (<ucw:a 
	   :action (call-component 
		    $holder (make-instance 'list-agencies))
	   "Consultants")))))

(defcomponent user-links (links)
  ())

(defmethod render ((self user-links))
  ;(call-next-method)
  (<:table 
   :border 1 :style "margin:auto;"
   (<:tr
    (<:td (<ucw:a 
	   :action (call-component 
		    $holder (make-instance 'user-summary))
	   "My Summary"))
    (<:td (<ucw:a 
	   :action (call-component 
		    $holder (make-instance 'my-projects))
	   "My Projects"))
    (<:td (<ucw:a 
	   :action (call-component 
		    $holder (make-instance 'my-files))
	   "My Working Files")))))

(defclass project-component ()
  ((project-id :accessor project-id :initarg :project-id)))

(defcomponent project-links (project-component)
  ())

(defmethod render ((self project-links))
  (let ((prj-id (project-id self)))
  (<:table 
   :border 1 :style "margin:auto;"
   (<:tr 
    (<:td (<ucw:a 
	   :action (call-component 
		    $holder (make-instance 'project-summary
					   :project-id prj-id))
	   "Project Summary"))
    (<:td (<ucw:a 
	   :action (call-component 
		    $holder (make-instance 'get-resource
					   :project-id prj-id))
	   "Add File"))
    (<:td (<ucw:a 
	   :action (call-component 
		    $holder (make-instance 'view-project-files
					   :project-id prj-id))
	   "View Files"))))))

(defun get-user (username)
  (let* ((result 
	  (car (clsql:select 
		[slot-value 'employee 'id]
		[slot-value 'employee 'folder]
		[slot-value 'employee 'level]
		[slot-value 'employee 'dept-id]
		[slot-value 'employee 'desig-id]
		[slot-value 'employee 'manager-id]
		[slot-value 'employee 'person-id]		   
		:from [|employees|]
		:where
		[=[slot-value 'employee 'username] username]
		:limit 1 :flatp t :field-names nil))))
    (when result
      (let ((user (make-instance 'employee
				 :id (first result)
				 :username username
				 :folder (second result)
				 :level (third result)
				 :dept-id (fourth result)
				 :desig-id (fifth result)
				 :manager-id (sixth result)
				 :person-id (seventh result))))
	user))))
