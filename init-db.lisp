;;;; This file is part of Aspiwork Project Data Management Server
;;;; Name:          init-db.lisp.lisp
;;;; Purpose:       creates database tables
;;;; Programmer:    Aashish R Lokhande
;;;; Copyright (C) 2010,2011 Aashish R Lokhande
;;;; All rights reserved.
;;;;
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions
;;;; are met:
;;;; 1. Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 2. Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;; 3. Neither the name of the author nor the names of its contributors
;;;;    may be used to endorse or promote products derived from this software
;;;;    without specific prior written permission.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
;;;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
;;;; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
;;;; OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;;;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
;;;; LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
;;;; OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
;;;; SUCH DAMAGE.

(clsql:create-view-from-class 'person)
(clsql:create-view-from-class 'address)
(clsql:create-view-from-class 'company)
(clsql:create-view-from-class 'designation)
(clsql:create-view-from-class 'department)
(clsql:create-view-from-class 'employee)
(clsql:create-view-from-class 'project)
(clsql:create-view-from-class 'team)
(clsql:create-view-from-class 'customer)
(clsql:create-view-from-class 'agency)
(clsql:create-view-from-class 'project-agency)
(clsql:create-view-from-class 'contact-person)
(clsql:create-view-from-class 'resource)
(clsql:create-view-from-class 'resource-category)
(clsql:create-view-from-class 'file-type)
(clsql:create-view-from-class 'current-file)
(clsql:create-view-from-class 'project-section)
(clsql:create-view-from-class 'permission)
