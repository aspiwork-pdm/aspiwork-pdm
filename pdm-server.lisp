;;;; This file is part of Aspiwork Project Data Management Server
;;;; Name:          server.lisp
;;;; Purpose:       defines all database classes and link classes
;;;; Programmer:    Aashish R Lokhande
;;;; Copyright (C) 2010,2011 Aashish R Lokhande
;;;; All rights reserved.
;;;;
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions
;;;; are met:
;;;; 1. Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 2. Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;; 3. Neither the name of the author nor the names of its contributors
;;;;    may be used to endorse or promote products derived from this software
;;;;    without specific prior written permission.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
;;;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
;;;; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
;;;; OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;;;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
;;;; LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
;;;; OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
;;;; SUCH DAMAGE.

(defun make-aspiwork-pdm-backend ()
  (make-backend
   :mod-lisp
   :host "localhost"
   :port 8011))

(defclass aspiwork-pdm-server (standard-server)
  ())

(defun make-aspiwork-pdm-server ()
  (make-instance
   'aspiwork-pdm-server
   :backend (make-aspiwork-pdm-backend)))

(defvar *aspiwork-pdm-server* (make-aspiwork-pdm-server))

(defclass aspiwork-pdm-application (standard-application
				cookie-session-application-mixin)
  ()
  (:default-initargs
   :url-prefix "/aspiwork-pdm/"))

(defparameter *aspiwork-pdm-application*
  (make-instance 'aspiwork-pdm-application))

(register-application *aspiwork-pdm-server* *aspiwork-pdm-application*)

(defun start-aspiwork-pdm ()
  (startup-server *aspiwork-pdm-server*))

(defun stop-aspiwork-pdm ()
  (shutdown-server *aspiwork-pdm-server*))
