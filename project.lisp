;;;; This file is part of Aspiwork Project Data Management Server
;;;; Name:          project.lisp
;;;; Purpose:       project & configuration
;;;; Programmer:    Aashish R Lokhande
;;;; Copyright (C) 2010,2011 Aashish R Lokhande
;;;; All rights reserved.
;;;;
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions
;;;; are met:
;;;; 1. Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 2. Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;; 3. Neither the name of the author nor the names of its contributors
;;;;    may be used to endorse or promote products derived from this software
;;;;    without specific prior written permission.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
;;;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
;;;; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
;;;; OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;;;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
;;;; LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
;;;; OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
;;;; SUCH DAMAGE.
(defcomponent project-manipulator ()
  ((project :accessor project :initarg :project :backtrack t
            :initform (make-instance 'project
				     :dept-id 0
				     :manager-id 0))))

(defcomponent show-project (project-manipulator)
  ())

(defmethod render ((self show-project))
  (let ((tproject (project self)))
    (<:p (<:ah "Basic Project Information"))
    (in-table
     ((<:label "Project Code") (<:ah (code tproject)))
     ((<:label "Project Name") (<:ah (name tproject)))     
     ((<:label "Active") (<:ah (active tproject)))
     ((<:label "Project-Dept") (<:ah (prj-dept tproject)))
     ((<:label "Project Manager") (<:ah (prj-manager tproject)))
     ((<:label "Start-Date") (<:ah (start-date tproject)))
     ((<:label "Finish-Date") (<:ah (finish-date tproject)))
     ((<:label "Info") (<:ah (info tproject))))
    (render (make-instance 'show-address 
			   :address (address-from-id 
						 (address-id tproject))))))

(defcomponent get-project (project-manipulator holder)
  ()
  (:default-initargs :message "Project Editor"))

(defmethod render :wrapping ((self get-project))
  (call-next-method)
  (let* ((user $user)
	 (tproject (project self))
	 (taddress (if (slot-boundp tproject 'id)
		       (address tproject)
		       (make-instance 'address)))
	 (dept-list (dept-ddlist))
	 (man-list (manager-ddlist))
	 (sdate (start-date tproject))
	 (fdate (finish-date tproject)))
    (<ucw:form 
     :action (update-project self tproject taddress user)
     :method "post"
     (<:p (<:ah "Basic Project Information"))
     (multiple-value-bind (sd sm sy)(clsql:decode-date sdate)
       (multiple-value-bind (fd fm fy)(clsql:decode-date fdate)
	 (<:table
	  :border 1 :style "margin:auto;"
	  (unless (slot-boundp tproject 'id)
	    (<:tr
	     (<:td (<:label "Code"))
	     (<:td (<ucw:input :type "text" 
			       :accessor (code tproject))))
	    (<:tr
	     (<:td (<:label "Name"))
	     (<:td (<ucw:input :type "text" 
			       :accessor (name tproject)))))
	  (<:tr
	   (<:td (<:label "Department"))
	   (<:td (<ucw:select :accessor (dept-id tproject)
			      (<ucw:option :value 0 "Department")
			      (loop for (id b) in dept-list
				 do (<ucw:option :value id (<:ah b))))))
	  (<:tr
	   (<:td (<:label "Manager"))
	   (<:td (<ucw:select :accessor (manager-id tproject)
			      (<ucw:option :value 0 "Manager")
			      (loop for (id b) in man-list
				 do (<ucw:option :value id (<:ah b))))))
	  (<:tr
	   (<:td (<:label "Start Date"))
	   (<:td "D :" (<ucw:select :accessor sd
				    (dotimes (x 31) 
				      (<ucw:option :value (+ 1 x) 
						   (<:ah (+ 1 x)))))
		 " M :" (<ucw:select :accessor sm
				     (dotimes (x 12) 
				       (<ucw:option :value (+ 1 x) 
						    (<:ah (+ 1 x)))))
		 " Y :" (<ucw:select :accessor sy
				     (do ((x 1950 (1+ x))) ((>= x 2020)) 
				       (<ucw:option :value x (<:ah x))))
		 (<ucw:input :type "hidden"
			     :writer (lambda (val)
				       (declare (ignore val))
				       (setf (slot-value tproject 'start-date)
					     (clsql:make-date 
					      :day sd
					      :month sm
					      :year sy))))))
	  (<:tr
	   (<:td (<:label "Finish Date"))
	   (<:td "D :" (<ucw:select :accessor fd
				    (dotimes (x 31) 
				      (<ucw:option :value (+ 1 x) 
						   (<:ah (+ 1 x)))))
		 " M :" (<ucw:select :accessor fm
				     (dotimes (x 12) 
				       (<ucw:option :value (+ 1 x) 
						    (<:ah (+ 1 x)))))
		 " Y :" (<ucw:select :accessor fy
				     (do ((x 1950 (1+ x))) ((>= x 2020)) 
				       (<ucw:option :value x (<:ah x))))
		 (<ucw:input :type "hidden"
			     :writer (lambda (val)
				       (declare (ignore val))
					 (setf (slot-value tproject 'finish-date)
					       (clsql:make-date  :day fd :month fm 
								 :year fy))))))
	  (<:tr
	   (<:td (<:label "Info"))
	   (<:td (<ucw:input :type "text" 
		       :accessor (info tproject)))))
	 (<:p (<:ah "Address"))
	 (render (make-instance 'get-address :address taddress))
	 (<:table 
	  :style "margin:auto;"
	  (<:tr 
	   (<:td :align "center"
		 (<:input :type "submit" :value "Submit" )))))))))

(defaction update-project ((self get-project)(tproject project) 
			   (taddress address)(user employee))
  (let* ((prj-errors (validate-project tproject))
	 (ad-errors (validate-address taddress))
	 (all-errors (concatenate 'string prj-errors ad-errors)))
    (if (string= "" all-errors)
	(progn
	  (insert-update-address taddress)
	  (if (slot-boundp tproject 'id)
	      (progn		
		(setf (slot-value tproject 'modifier-id) (id user))
		(setf (slot-value tproject 'modified-date) (clsql:get-time)))
	      (progn
		(setf (slot-value tproject 'address-id) (id taddress))
		(setf (slot-value tproject 'creater-id) (id user))
		(setf (slot-value tproject 'created-date) (clsql:get-time))))
	  (insert-update-project tproject)
	  (insert-project-manager 
	   (id tproject) (manager-id tproject))
	  (answer))
	(setf (message self) (concatenate 'string "Errors: " all-errors)))))
	
(defgeneric validate-project (project)(:documentation ""))

(defmethod validate-project ((project project))
  (let* ((nm "") (dp "") (mn ""))
    (progn
      (unless (slot-boundp project 'id)
	(setf (slot-value project 'name) (sanitize-input (name project)))
	(setf (slot-value project 'code) (sanitize-input (code project)))
	(if (clsql:select [id] :from [projects]
			  :where 
			  [=[name](name project)] :limit 1)
	    (return-from validate-project "Duplicate Name, "))
	(if (clsql:select [id] :from [projects]
			  :where 
			  [=[code](code project)] :limit 1)
	    (return-from validate-project "Duplicate Code, "))
	(if (and (string= "" (name project))
		 (string= "" (code project)))
	    (setf nm "Name/Code Empty, ")))
      (setf (slot-value project 'dept-id) (qp-to-int (dept-id project)))
      (if (= 0 (dept-id project))
	  (setf dp "Department, "))
      (setf (slot-value project 'manager-id) (qp-to-int (manager-id project)))
      (if (= 0 (manager-id project))
	  (setf mn "Manager, ")))
    (concatenate 'string nm dp mn)))

(defcomponent project-config (project-manipulator holder)
  ()
  (:default-initargs :message "Project Configuration Editor"))

(defmethod render :wrapping ((self project-config))
  (call-next-method)
  (let* ((user $user)
	 (tproject (project self))
	 (prjid (id tproject))
	 (emp-id 0)(sec-code "")(sec-name "")
	 (sec-info "")(agency-id 0)(cs-id 0))
    (<:ah (project-name-from-id prjid))
    (<:hr)
    (<:p "Project Sections")
    (<:table 
     :border 1 :style "margin:auto;"
     (<:tr (<:th "Code") (<:th "Name"))
     (loop for (id a b) in (sections-from-prjid prjid)
	do (<:tr (<:td (<:ah a))(<:td (<:ah b)))))
    (<:p "")
    (<ucw:form 
     :action (add-section-to-project self prjid sec-code sec-name sec-info)
     :method "post"	       
     (in-table
      ((<:label "Code")
       (<ucw:input :type "text" :value ""
		   :accessor sec-code))
      ((<:label "Name")
       (<ucw:input :type "text" :value ""
		   :accessor sec-name))
      ((<:label "Info")
       (<ucw:input :type "text" :value ""
		   :accessor sec-info))
      ((<:label " ")
       (<:input :type "submit" :value "Add Section"))))
    (<:hr)
    (<:p "Project Team")
    (<:table 
     :border 1 :style "margin:auto;"
     (<:tr (<:th "Member Name"))
     (loop for (id a) in (team-from-prjid prjid)
	do (<:tr (<:td (<:ah a)))))
    (<:p "")
    (<ucw:form 
     :action (add-member-to-project self prjid emp-id)
     :method "post"	       
     (<:table
      :border 1 :style "margin:auto;"
      (<:tr (<:td  "Employees in Dept: ")
	    (<:td (<ucw:select 
		   :accessor emp-id
		   (<ucw:option :value 0 "Employee")
		   (loop for (id b) in (emplist-from-deptid (dept-id tproject))
		      do (<ucw:option :value id (<:ah b))))))
      (<:tr 
       (<:td :align "center" :colspan 2
	     (<:input :type "submit" :value "Add Member")))))
    (<:hr)
    (<:p "Client/Customer:" )
    (if (customer-id tproject)
	(<:ah (customer-name-from-id (customer-id tproject)))
	(<ucw:form 
	 :action (add-customer self prjid cs-id)
	 :method "post"
	 (<:table
	  :border 1 :style "margin:auto;"
	  (<:tr (<:td "Firm Name: ")
		(<:td (<ucw:select 
		       :accessor cs-id
		       (<ucw:option :value 0 "Customer")
		       (loop for (id b) in (customer-ddlist)
			  do (<ucw:option :value id (<:ah b))))))
	  (<:tr 
	   (<:td :align "center" :colspan 2
		 (<:input :type "submit" :value "Add Customer"))))))
    (<:hr)
    (<:p "Consultants/Agencies :" )
    (<:table 
     :border 1 :style "margin:auto;"
     (<:tr (<:th "Firm Name"))
     (loop for (a) in (agencies-from-prjid prjid)
	do (<:tr (<:td (<:ah a)))))
    (<:p "")
    (<ucw:form 
     :action (add-agency self prjid agency-id)
     :method "post"	       
     (<:table
      :border 1 :style "margin:auto;"
      (<:tr (<:td "Firm Name: ")
	    (<:td (<ucw:select 
		   :accessor agency-id
		   (<ucw:option :value 0 "Firm")
		   (loop for (id b) in (agency-ddlist)
		      do (<ucw:option :value id (<:ah b))))))
      (<:tr 
       (<:td :align "center" :colspan 2
	     (<:input :type "submit" :value "Add Firm")))))))

(defaction add-customer ((self project-config) prjid cs-id) 
  (let ((cst-id (qp-to-int cs-id)))
    (clsql:update-records 
     [projects]
     :av-pairs 
     `((customer_id ,cst-id))
     :where
     [= [slot-value 'project 'id] prjid])))

(defaction add-agency ((self project-config) prjid ag-id)
  (let* ((agc-id (qp-to-int ag-id))
	 (duplicate (car (clsql:select [agency-id] :from [project-agencies]
				       :where [and									   
				       [= [project-id] prjid]
				       [= [agency-id] agc-id]]
				       :limit 1
				       :flatp t))))
    (if duplicate
	(setf (message self) "Duplicate Firm")
	(clsql:insert-records 
	 :into [project-agencies]
	 :av-pairs 
	 `((project_id ,prjid)
	   (agency_id ,agc-id))))))

(defaction add-section-to-project ((self project-config)
				   prj-id sec-code sec-name sec-info)
  (let* ((name (sanitize-input sec-name))
	 (code (sanitize-input sec-code))
	 (info (sanitize-input sec-info))
	 (duplicate (car (clsql:select [id] :from [project-sections]
				       :where [and
				       [= [project-id] prj-id]
				       [= [name] name]
				       [= [code] code]]
				       :limit 1
				       :flatp t))))
    (if duplicate 
	(setf (message self) "Duplicate Section.")
	(if (and (string= "" name) (string= "" code))
	    (setf (message self) "Name/code Empty.")
	    (clsql:insert-records 
	     :into [project_sections]
	     :av-pairs 
	     `((project_id ,prj-id)
	       (code ,sec-code)
	       (name ,sec-name)
	       (info ,sec-info)))))))

(defaction add-member-to-project ((self project-config) prj-id emp-id)
  (let* ((empid (qp-to-int emp-id))
	 (duplicate (car (clsql:select [member-id] :from [teams]
				       :where [and				       
				       [= [project-id] prj-id]
				       [= [member-id] empid]]
				       :limit 1
				       :flatp t))))
    (if duplicate
	(setf (message self) "Duplicate Member")
	(clsql:insert-records 
	 :into [teams]
	 :av-pairs 
	 `((project_id ,prj-id)
	   (member_id ,empid))))))

(defun insert-project-manager (prj-id empid)
  (clsql:insert-records 
	 :into [teams]
	 :av-pairs 
	 `((project_id ,prj-id)
	   (member_id ,empid))))
    
(defaction view-project (id)
  (call 'show-project :project (get-project-from-id id)))

(defaction edit-project (id)
  (call 'get-project :project (get-project-from-id id)))

(defcomponent list-projects (holder)
  ()
  (:default-initargs :message "Project List"))

(defmethod render :wrapping ((self list-projects))
  (call-next-method)
  (let* ((user $user)
	 (prj-list (project-tlist))
	 (prjid 0))
    (<ucw:a 
     :action (call-component 
	      $holder (make-instance 'get-project))
     "New Project")
    (<:hr)
    (<ucw:form 
     :action (goto-project-config prjid)
    (<:table 
     :border 1 :style "margin:auto;"
     (<:tr (<:th "Code") (<:th "Name") (<:th "Dept")
	   (<:th "Manager") (<:th "Start") (<:th "Finish")
	   (<:th "Select"))
     (loop for (id cd nm dp mn sd fd) in prj-list
	do (<:tr (<:td (<:ah cd))(<:td (<:ah nm))
		 (<:td (<:ah (dept-name-from-id dp)))
		 (<:td (<:ah (emp-name-from-id mn)))
		 (<:td (<:ah sd))(<:td(<:ah fd))
		 (<:td (<ucw:input :type "radio" :name "selected"
				  :accessor prjid :value id))))
     (<:tr 
       (<:td :align "center" :colspan 7
	     (<:input :type "submit" :value "Project Configuration")))))))

(defaction goto-project-config (id)
  (call-component
   $holder 
   (make-instance 
    'project-config 
    :project (project-from-id id))))

(defun next-project-id ()
  (if (= 0 (car (clsql:select [count [*]] :from [projects] :flatp t)))
      1
      (+ 1 (car 
	    (clsql:select [max [id]] 
			  :from [projects]
			  :flatp t)))))

(defun insert-update-project (obj)
  (if (slot-boundp obj 'id)
      (clsql:update-records-from-instance obj)
      (clsql:with-transaction ()	  
		(setf (id obj) (next-project-id))
		(clsql:update-records-from-instance obj))))

(defun project-from-id (prj-id);object
  (car (clsql:select 'project   
		:where [=[slot-value 'project 'id] prj-id]
		:flatp t
		:refresh t)))

(defun project-tlist ();table
  (clsql:select
   [slot-value 'project 'id]
   [slot-value 'project 'code]
   [slot-value 'project 'name]
   [slot-value 'project 'dept-id]
   [slot-value 'project 'manager-id]
   [slot-value 'project 'start-date]
   [slot-value 'project 'finish-date]    
   :from [projects]   
   :field-names nil))



(defun team-from-prjid (prj-id)
  (clsql:select
   [slot-value 'team 'member-id]
   [concat
   [slot-value 'person 'first-name] " "
   [slot-value 'person 'middle-name] " "
   [slot-value 'person 'last-name]]
   :from '([teams][employees][persons])
   :where
   [and
   [=[slot-value 'team 'project-id] prj-id]
   [=[slot-value 'team 'member-id][slot-value 'employee 'id]]
   [=[slot-value 'person 'id][slot-value 'employee 'person-id]]
   ]
   :field-names nil))

(defun project-name-from-id (prj-id)
  (car (clsql:select   
   [slot-value 'project 'name]   
   :from [|projects|]
   :where      
   [=[slot-value 'project 'id] prj-id]   
   :field-names nil
   :flatp t )))

(defun sections-from-prjid (prj-id);for table
  (clsql:select
   [slot-value 'project-section 'id]
   [slot-value 'project-section 'code]
   [slot-value 'project-section 'name]
   :from [project_sections]
   :where [=[slot-value 'project-section 'project-id] prj-id]
   :field-names nil))

(defun project-code-from-id (prj-id)
  (car (clsql:select   
   [slot-value 'project 'code]   
   :from [|projects|]
   :where      
   [=[slot-value 'project 'id] prj-id]   
   :field-names nil
   :flatp t
   )))

(defun section-code-from-id (id)
  (car (clsql:select   
   [slot-value 'project-section 'code]   
   :from [project_sections]
   :where 
   [=[slot-value 'project-section 'id] id]   
   :field-names nil
   :flatp t
   )))

(defun section-name-from-id (id)
  (car (clsql:select   
   [slot-value 'project-section 'name]   
   :from [project_sections]
   :where      
   [=[slot-value 'project-section 'id] id]   
   :field-names nil
   :flatp t
   )))