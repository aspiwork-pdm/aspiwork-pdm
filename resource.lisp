;;;; This file is part of Aspiwork Project Data Management Server
;;;; Name:          resource.lisp
;;;; Purpose:       
;;;; Programmer:    Aashish R Lokhande
;;;; Copyright (C) 2010,2011 Aashish R Lokhande
;;;; All rights reserved.
;;;;
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions
;;;; are met:
;;;; 1. Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 2. Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;; 3. Neither the name of the author nor the names of its contributors
;;;;    may be used to endorse or promote products derived from this software
;;;;    without specific prior written permission.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
;;;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
;;;; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
;;;; OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;;;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
;;;; LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
;;;; OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
;;;; SUCH DAMAGE.

(defcomponent resource-manipulator (project-component)
  ((resource :accessor resource :initarg :resource :backtrack t
			 :initform (make-instance 'resource 
						  :section-id 0
						  :category-id 0
						  :type-id 0))))

(defcomponent show-resource (resource-manipulator)
  ())

(defmethod render ((self show-resource))
  (let ((tresource (resource self)))
    (<:p (<:ah "Basic File Information"))
    (in-table
     ((<:label "Code") (<:ah (code tresource)))
     ((<:label "Name") (<:ah (name tresource)))
     ((<:label "Project") (<:ah (project-id tresource)))
     ((<:label "Project Section") (<:ah (section-id tresource)))
     ((<:label "Category") (<:ah (category-id tresource)))
     ((<:label "Type") (<:ah (type-id tresource)))     
     ((<:label "Version") (<:ah (version tresource)))
     ((<:label "Revision") (<:ah (revision tresource)))     
     ((<:label "Description") (<:ah (info tresource))) 
     ((<:label "Comment") (<:ah (comment tresource))))))

(defcomponent get-resource (resource-manipulator holder)
  ()
  (:default-initargs :message "Resource Editor"))

(defmethod render :wrapping ((self get-resource))
  (call-next-method)
  (let* ((user $user)
	 (tresource (resource self))
	 (prj-id (project-id self))
	 (user-id (id user))
	 (upload-folder (concatenate 'string (folder user) "/upload/"))
	 (file-list (local-user-files upload-folder)))
    (if (car file-list)
	(<ucw:form 
	 :action (insert-new-resource self user tresource)
	 :method "post"
	 (<:p "Files from my PC")
	 (<:table
	  :border 1 :style "margin:auto;"
	  (<:tr (<:th  "Name") (<:th "Select"))
	  (loop for name in file-list
	     do (<:tr (<:td (<:ah name))
		      (<:td (<ucw:input :type "radio" :name "selected"
					:accessor (name tresource) :value name)))))
	 (<:p "File Details" )
	 (in-table
	  ((<:label "Project Section")
	   (<ucw:select :accessor (section-id tresource)
			(<ucw:option :value 0 "Section")
			(loop for (id b) in (seclist-from-prjid prj-id)
			   do (<ucw:option :value id (<:ah b)))))
	  ((<:label "Category")
	   (<ucw:select :accessor (category-id tresource)
			(<ucw:option :value 0 "Category")
			(loop for (id b) in (res-catlist-for-user user-id)
			   do (<ucw:option :value id (<:ah b)))))
	  ((<:label "Type")
	   (<ucw:select :accessor (type-id tresource)
			(<ucw:option :value 0 "Type")
			(loop for (id b) in (filetype-ddlist)
			   do (<ucw:option :value id (<:ah b)))))
	  ((<:label "Version")
	   (<ucw:input :type "text" 
		       :accessor (version tresource)))
	  ((<:label "Revision")
	   (<ucw:input :type "text" 
		       :accessor (revision tresource))
	   (<ucw:input :type "hidden"
		       :writer 
		       (lambda (val)
			 (declare (ignore val))
			 (setf (slot-value tresource 'project-id) 
			       (qp-to-int prj-id)))))
	  ((<:label "Description")
	   (<ucw:input :type "text" 
		       :accessor (info tresource)))
	  ((<:label "Comment")
	   (<ucw:input :type "text" 
		       :accessor (comment tresource))))
	 (<:table 
	  :style "margin:auto;"
	  (<:tr 
	   (<:td :align "right"
		 (<:input :type "submit" :value "Submit" )))))
	(<:ah "Your upload folder is unaccessible"))))

(defun local-user-files (upload-folder)
  (let ((raw-list (cl-fad:list-directory upload-folder)))
    (loop for x in raw-list 
       unless (cl-fad:directory-pathname-p x) collect (file-namestring x))))

(defgeneric validate-resource (resource)(:documentation ""))

(defmethod validate-resource ((tresource resource))
  (let* ((vr "") (rv ""))
    (progn
      (setf (slot-value tresource 'section-id)
	    (qp-to-int(section-id tresource)))
      (setf (slot-value tresource 'category-id)
	    (qp-to-int(category-id tresource)))
      (setf (slot-value tresource 'type-id)
	    (qp-to-int(type-id tresource)))
      (if (car (clsql:select [id] :from [resources] 
			     :where [and
			     [= [project-id] (project-id tresource)]
			     [= [section-id] (section-id tresource)]
			     [= [category-id] (category-id tresource)]
			     [= [type-id] (type-id tresource)]
			     [= [name] (name tresource)]]))
	  (return-from validate-resource "File already exists"))
      (setf (slot-value tresource 'version)
	    (qp-to-int(version tresource)))
      (if (numberp (version tresource))
	  (when (and (minusp (version tresource))
		     (> (version tresource) 10))
	    (setf vr "Version "))
	  (setf vr "Version "))
      (setf (slot-value tresource 'revision)
	    (qp-to-int(revision tresource)))
      (if (numberp (revision tresource))
	  (when (and (minusp (revision tresource)) 
		     (> (revision tresource) 10))
	    (setf rv "Revision "))
	  (setf rv "Revision "))
      (setf (slot-value tresource 'info)
	    (sanitize-input (info tresource)))
      (setf (slot-value tresource 'comment)
	    (sanitize-input (comment tresource))))
    (concatenate 'string vr rv)))

(defaction insert-new-resource ((self get-resource)(user employee)
				(tresource resource))
  (let ((input-errors (validate-resource tresource)))
    (if (string= "" input-errors)
	(let* ((new-name (schematic-name tresource))
	       (base-folder (concatenate 
			     'string
			     (remove #\space 
				     (project-name-from-id 
				      (project-id tresource))) 
			     "/" (remove #\space 
					 (section-name-from-id 
					  (section-id tresource))) 
			     "/" (remove #\space 
					 (res-cat-name-from-id 
					  (category-id tresource))) "/" ))
	       (source (merge-pathnames (name tresource)
					(merge-pathnames "upload/" (folder user))))
	       (destination (merge-pathnames 
			     new-name (merge-pathnames 
				       base-folder
				       (company-folder)))))
	  (progn
	    (setf (slot-value tresource 'current)  new-name)
	    (setf (slot-value tresource 'ref-id) 0)
	    (setf (slot-value tresource 'folder) (namestring base-folder))
	    (setf (slot-value tresource 'created-date) (clsql:get-time))
	    (setf (slot-value tresource 'creater-id) (id user))
	    (ensure-directories-exist (directory-namestring destination))
	    (cl-fad:copy-file source destination)
	    (insert-update-resfile tresource)
	    (answer)))
	(setf (message self) (concatenate 'string "Errors: " input-errors)))))

(defaction fetch-file ((self view-file-revisions) (user employee) file-id)
  (if (car (clsql:select [resource-id] 
			 :from [current-files]
			 :where [and
			 [= [resource-id] file-id]
			 [= [accessor-id] (id user)]
			 ]))
      (setf (message self) "File already with exists on your PC!")
      (let* ((user-folder (folder user))
	     (tresource (resource-from-id file-id))
	     (source-path (concatenate 'string
				       (company-folder)
				       (folder tresource)
				       (current tresource)))
	     (dest-path (concatenate 'string
				     user-folder 
				     (folder tresource)
				     (current tresource))))
	(ensure-directories-exist (directory-namestring dest-path))
	(cl-fad:copy-file source-path dest-path :overwrite t)	        
	(clsql:insert-records 
	 :into [current_files]
	 :av-pairs
	 `((resource_id ,file-id)
	   (accessor_id ,(id user))
	   (accessed_date ,(clsql:get-time))
	   (central_location ,source-path)
	   (fetch_location ,dest-path)))
	(clsql:update-records 
	 [resources]
	 :av-pairs
	 `((in_use ,(+ 1 (in-use tresource))))
	 :where [= [slot-value 'resource 'id] file-id])	
	(answer))))      

(defaction update-file ((self my-files) (user employee) 
			file-id keep comment)
  (let* ((user-id (id user))
	 (kp (qp-to-int keep))
	 (current-file 
	  (car (clsql:select 'current-file
			     :from [current_files]
			     :where [and
			     [= [resource-id] file-id]
			     [= [accessor-id] user-id]]
			     :flatp t
			     :refresh t)))
	 (new-save-count (+ 1 (car 
			       (clsql:select
				[slot-value 'resource 'save-count]
				:from [resources]
				:where 
				[= [slot-value 'resource 'id] file-id]
				:flatp t))))
	 (old (central-location current-file))
	 (new (fetch-location current-file)))
    (cl-fad:copy-file new old :overwrite t)
    (clsql:update-records
     [resources]
     :av-pairs
     `((save_count ,new-save-count)
       (comment ,comment)
       (modifier_id ,user-id)
       (modified_date ,(clsql:get-time)))
     :where [= [slot-value 'resource 'id] file-id])
    (unless (= 1 kp)
      (clsql:delete-records :from [current_files]
			    :where [and
			    [= [resource-id] file-id]
			    [= [accessor-id] user-id]]))    
    (answer)))

(defaction revise-file ((self my-files) (user employee) 
			file-id v-r keep comment) 
  (let* ((user-id (id user))	  
	 (current-file 
	  (car (clsql:select 'current-file
			     :from [current_files]
			     :where [and
			     [= [resource-id] file-id]
			     [= [accessor-id] user-id]]
			     :flatp t
			     :refresh t)))
	 (old-file (resource-from-id file-id))
	 (new-file (make-instance 
		    'resource 
		    :project-id (project-id old-file)
		    :category-id (category-id old-file)
		    :type-id (type-id old-file)
		    :section-id (section-id old-file)
		    :name (name old-file)
		    :ref-id (if (= 0 (ref-id old-file))(id old-file)(ref-id old-file))
		    :version (if (= 2 v-r)(1+ (version old-file))(version old-file))
		    :revision (if (= 1 v-r)(1+ (revision old-file)) 0)
		    :save-count (+ 1 (save-count old-file))
		    :creator-id user-id
		    :folder (folder old-file)
		    :info (info old-file)))
	 (new-name (schematic-name new-file))
	 (destn (concatenate 
		 'string 
		 (directory-namestring (central-location current-file)) 
		 new-name)))
    (cl-fad:copy-file (fetch-location current-file) destn :overwrite t)
    (cl-fad:copy-file (fetch-location current-file) 
		      (merge-pathnames (file-namestring destn)
				       (directory-namestring
					(fetch-location current-file))) 
		      :overwrite t)
    (setf (current new-file) new-name)
    (setf (comment new-file) comment)
    (insert-update-resfile new-file)       
    (if (string= "1" keep)
	(let ((newfetchloc (concatenate 'string (directory-namestring 
						 (fetch-location current-file)) 
					new-name)))
	  (clsql:update-records [current_files]
				:av-pairs
				`((resource_id ,(id new-file))
				  (fetch_location ,newfetchloc)
				  (central_location ,destn))
				:where [and
				[= [resource-id] file-id]
				[= [accessor-id] user-id]]))
	(clsql:delete-records :from [current_files]
			      :where [and
			      [= [resource-id] file-id]
			      [= [accessor-id] user-id]]))
    (answer)))

(defun schematic-name (tresource)
  (concatenate 'string 
	       (remove #\space 
		       (project-code-from-id 
			(project-id tresource))) 
	       "-"
	       (remove #\space 
		       (section-code-from-id 
			(section-id tresource))) 
	       "-"
	       (remove #\space 
		       (res-cat-code-from-id 
			(category-id tresource)))
	       "-" 
	       (pathname-name (name tresource)) "-"
	       (princ-to-string (version tresource)) "."
	       (princ-to-string (revision tresource)) "." 
	       (pathname-type (name tresource))))

(defun extract-revision (name)
  (parse-integer (pathname-type (pathname-name name))))

(defun extract-version (name)
  (parse-integer (pathname-type (pathname-name (pathname-name name)))))

(defun extract-pure-name (name)
  (pathname-name (pathname-name (pathname-name name))))

(defun company-folder ()
  (car (clsql:select [folder] :from [companies] :where [= [id] 1]
		     :flatp t)))

(defun next-resfile-id ()
  (if (= 0 (car (clsql:select [count [*]] :from [resources] :flatp t)))
      1
      (+ 1 (car 
	    (clsql:select [max [id]] 
			  :from [resources]
			  :flatp t
			  :field-names nil)))))

(defun insert-update-resfile (obj)
  (if (slot-boundp obj 'id)
      (clsql:update-records-from-instance obj)
      (clsql:with-transaction ()	  
	(setf (id obj) (next-resfile-id))
	(clsql:update-records-from-instance obj))))

(defun resource-from-id (res-id);object 
  (car (clsql:select 'resource   
		:where [=[slot-value 'resource 'id] res-id]
		:flatp t
		:refresh t)))