;;;; This file is part of Aspiwork Project Data Management Server
;;;; Name:          address.lisp
;;;; Purpose:       
;;;; Programmer:    Aashish R Lokhande
;;;; Copyright (C) 2010,2011 Aashish R Lokhande
;;;; All rights reserved.
;;;;
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions
;;;; are met:
;;;; 1. Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 2. Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;; 3. Neither the name of the author nor the names of its contributors
;;;;    may be used to endorse or promote products derived from this software
;;;;    without specific prior written permission.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
;;;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
;;;; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
;;;; OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;;;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
;;;; LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
;;;; OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
;;;; SUCH DAMAGE.

(defcomponent address-manipulator ()
  ((address :accessor address :initarg :address :backtrack t
	   :initform (make-instance 'address))))

(defcomponent show-address (address-manipulator)
  ())

(defmethod render ((self show-address))
  (let ((taddress (address self)))
    (<:p (<:ah "Address"))
    (in-table 
     ((<:label "Line1:") (<:ah (line1 taddress)))
     ((<:label "Line2:") (<:ah (line2 taddress)))
     ((<:label "State:") (<:ah (state taddress)))
     ((<:label "Pin:") (<:ah (pincode taddress)))
     ((<:label "Phone:") (<:ah (phone taddress)))
     ((<:label "Mobile:") (<:ah (mobile taddress)))
     ((<:label "Fax:") (<:ah (fax taddress)))
     ((<:label "Email:") (<:ah (email taddress)))
     ((<:label "Website:") (<:ah (website taddress))))))

(defcomponent get-address (address-manipulator)
  ())

(defmethod render ((self get-address))
  (let ((taddress (address self)))    
    (in-table 
     ((<:label "Line1:")
      (<ucw:input :type "text" 
		  :accessor (line1 taddress)))
     ((<:label "Line2:")
      (<ucw:input :type "text" 
		  :accessor (line2 taddress)))
     ((<:label "State:")
      (<ucw:input :type "text" 
		  :accessor (state taddress)))
     ((<:label "Pin:")
      (<ucw:input :type "text" 
		  :accessor (pincode taddress)))
     ((<:label "Phone:")
      (<ucw:input :type "text" 
		  :accessor (phone taddress)))
     ((<:label "Mobile:")
      (<ucw:input :type "text" 
		  :accessor (mobile taddress)))
     ((<:label "Fax:")
      (<ucw:input :type "text" 
		  :accessor (fax taddress)))
     ((<:label "Email:")
      (<ucw:input :type "text" 
		  :accessor (email taddress)))
     ((<:label "Website:")
      (<ucw:input :type "text" 
		  :accessor (website taddress))))))    

(defgeneric validate-address (address)
  (:documentation ""))

(defmethod validate-address ((address address))
  (let ((st "")(pn "")(ph "")(mb "")(fx "")(em ""))
    (progn
      (setf (slot-value address 'line1) (sanitize-input (line1 address)))
      (setf (slot-value address 'line2) (sanitize-input (line2 address)))      
      (when (position-if-not #'alpha-char-p (state address))
	  (setf st "State "))
      (when (position-if-not #'digit-char-p (pincode address))
	  (setf pn "Pin "))
      (when (position-if-not #'digit-char-p (phone address))
	  (setf ph "Phone "))
      (when (position-if-not #'digit-char-p (mobile address))
	  (setf mb "Mobile "))
      (when (position-if-not #'digit-char-p (fax address))
	  (setf fx "Fax "))
      (if (or (not (find #\@ (email address))) 
	      (not (find #\. (email address))))
	  (setf em "Email ")))
   (concatenate 'string st pn ph mb fx em)))

(defun next-address-id ()
  (if (= 0 (car (clsql:select [count [*]] :from [addresses] :flatp t)))
      1
      (+ 1 (car 
	    (clsql:select [max [id]] 
			  :from [addresses]
			  :flatp t)))))

(defun insert-update-address (obj)
  (if (slot-boundp obj 'id)
      (clsql:update-records-from-instance obj)
      (clsql:with-transaction ()	  
	(setf (id obj) (next-address-id))
	(clsql:update-records-from-instance obj))))

(defun address-from-id (id)
  (car (clsql:select 'address
		      :where [= [id] id]
		      :flatp t)))
