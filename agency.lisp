;;;; This file is part of Aspiwork Project Data Management server
;;;; Name:          agency.lisp
;;;; Purpose:       
;;;; Programmer:    Aashish R Lokhande
;;;; Copyright (C) 2010,2011 Aashish R Lokhande
;;;; All rights reserved.
;;;;
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions
;;;; are met:
;;;; 1. Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 2. Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;; 3. Neither the name of the author nor the names of its contributors
;;;;    may be used to endorse or promote products derived from this software
;;;;    without specific prior written permission.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
;;;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
;;;; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
;;;; OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;;;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
;;;; LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
;;;; OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
;;;; SUCH DAMAGE.

(defcomponent agency-manipulator ()
  ((agency :accessor agency :initarg :agency :backtrack t
            :initform (make-instance 'agency))))

(defcomponent show-agency (agency-manipulator)
  ())

(defmethod render ((self show-agency))
  (let ((tagency (agency self)))
    (<:p (<:ah "Basic Agency Information"))
    (in-table
     ((<:label "Agency Name")
      (<:ah (name tagency)))
     ((<:label "Agency Code")
      (<:ah (code tagency)))
     ((<:label "Agency Info")
      (<:ah (info tagency))))
    (<:p (<:ah "Address"))
    (render 'show-address 
	    :address (address-from-id (address-id tagency)))))

(defcomponent get-agency (agency-manipulator holder)
  ()
  (:default-initargs :message "Agency Editor"))

(defmethod render :wrapping ((self get-agency))
  (call-next-method)
  (let* ((user $user)
	 (tagency (agency self))	
	 (taddress (if (slot-boundp tagency 'id)
		       (address tagency)
		       (make-instance 'address))))
    (<ucw:form :action (update-agency self tagency taddress user) 
	       :method "post"
	       (<:p "Basic Agency Information")
	       (in-table
		((<:label "Agency Code")
		 (<ucw:input :type "text" 
			     :accessor (code tagency)))
		((<:label "Agency Name")
		 (<ucw:input :type "text" 
			     :accessor (name tagency)))		
		((<:label "Agency Info")
		 (<ucw:input :type "text" 
			     :accessor (info tagency))))
	       (<:p (<:ah "Address"))
	       (render (make-instance 'get-address :address taddress))
	       (<:table 
		:style "margin:auto;"
		(<:tr 
		 (<:td :align "right"
		       (<:input :type "submit" :value "Submit" )))))))

(defaction update-agency ((self get-agency)(tagency agency)
			  (taddress address)(user employee))
  (let ((user $user))
  (if (string= "" (name tagency))
      (setf (message self) "Error: Name")  
      (progn
	(insert-update-address taddress)
	(if (slot-boundp tagency 'id)
	    (progn
	      (setf (modifier-id tagency) (id user))
	      (setf (modified-date tagency) (clsql:get-time)))
	    (progn
	      (setf (slot-value tagency 'address-id) (id taddress))
	      (setf (creater-id tagency) (id user))
	      (setf (created-date tagency) (clsql:get-time))))
	(insert-update-agency tagency)
	(answer)))))

(defaction view-agency (id)
  (call-component $holder (make-instance 
			   'show-agency :agency (agency-from-id id))))

(defaction edit-agency (id)
  (call-component $holder (make-instance 
			   'get-agency :agency (agency-from-id id))))

(defcomponent list-agencies (holder)
  ()(:default-initargs :message "Agency List"))

(defmethod render :wrapping ((lc list-agencies))
  (call-next-method)
  (let* ((user $user)
	 (ag-list (agency-tlist))
	 (ag-id 0))
    (<ucw:a 
     :action (call-component 
	      $holder (make-instance 'get-agency))
     "New Agency")
    (<:hr)
    (<ucw:form 
     :action (edit-agency cl-id)
     (<:table 
      :border 1 :style "margin:auto;"
      (<:tr (<:th "Code") (<:th "Name") (<:th "Select"))
      (loop for (id a b) in ag-list
	 do (<:tr (<:td (<:ah a)) (<:td (<:ah b))
		  (<:td (<ucw:input :type "radio" :name "selected"
				    :accessor ag-id :value id))))
      (<:tr 
       (<:td :align "right" :colspan 3
	     (<:input :type "submit" :value "Edit")))))))

(defun next-agency-id ()
  (if (= 0 (car (clsql:select [count [*]] :from [agencies] :flatp t)))
      1
      (+ 1 (car 
	    (clsql:select [max [id]] 
			  :from [agencies]
			  :flatp t)))))

(defun insert-update-agency (obj)
  (if (slot-boundp obj 'id)
      (clsql:update-records-from-instance obj)
      (clsql:with-transaction ()	  
	(setf (id obj) (next-agency-id))
	(clsql:update-records-from-instance obj))))

(defun agency-from-id (ag-id);object
  (car (clsql:select 'agency   
		:where [=[slot-value 'agency 'id] ag-id]
		:flatp t)))

(defun agency-name-from-id (ag-id);name string  
   (car (clsql:select [slot-value 'agency 'name]
		      :from [agencies]
		      :where [=[slot-value 'agency 'id] ag-id]
		      :field-names nil)))

(defun agency-tlist ();table
  (clsql:select
   [slot-value 'agency 'id] 
   [slot-value 'agency 'code]
   [slot-value 'agency 'name]   
   :from [agencies]
   :field-names nil))

(defun agencies-from-prjid (prj-id);name-table
  (clsql:select
   [slot-value 'agency 'name]      
   :from '([agencies] [project-agencies])
   :where [and
   [= [slot-value 'project-agency 'project-id] prj-id]
   [= [slot-value 'project-agency 'agency-id] [slot-value 'agency 'id]]]
   :field-names nil))

(defun agency-ddlist ();for drop-down-list
  (clsql:select 
   [slot-value 'agency 'id] 
   [slot-value 'agency 'name]   
   :from [agencies]
   :field-names nil))