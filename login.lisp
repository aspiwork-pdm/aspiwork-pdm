;;;; This file is part of Aspiwork Project Data Management Server
;;;; Name:          login.lisp
;;;; Purpose:       
;;;; Programmer:    Aashish R Lokhande
;;;; Copyright (C) 2010,2011 Aashish R Lokhande
;;;; All rights reserved.
;;;;
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions
;;;; are met:
;;;; 1. Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 2. Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;; 3. Neither the name of the author nor the names of its contributors
;;;;    may be used to endorse or promote products derived from this software
;;;;    without specific prior written permission.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
;;;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
;;;; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
;;;; OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;;;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
;;;; LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
;;;; OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
;;;; SUCH DAMAGE.

(defcomponent aspiwork-login (standard-window-component)
  ((message :accessor message
	    :initform "Aspiwork PDM Login" )
   (username :accessor username
	     :initform nil)
   (password :accessor password
	     :initform nil))
  (:default-initargs       
      :title "Aspiwork Project Data Management Suite"))

(defmethod render ((self aspiwork-login))  
  (<:div 
   :style "margin:auto; width:800px;"
   (<ucw:form :action (try-login self) :method "post"
	      (<:br )(<:br )(<:br )(<:br )(<:br )(<:br )(<:br )(<:br )
	      (<:table 
	       :border 1 :style "margin:auto;"
	       (<:tr (<:td :align "center" :colspan 2 (<:ah (message self))))
	       (<:tr (<:td :align "right" "Username")
		     (<:td :align "left"  
			   (<ucw:input :type "text"
				       :accessor (username self))))
	       (<:tr (<:td :align "right" "Password")
		     (<:td :align "left"  
			   (<ucw:input :type "password"
				       :accessor (password self))))
	       (<:tr (<:td :align "center" :colspan 2
			   (<:button :type "submit" "Login")))))))

(defaction try-login ((self aspiwork-login))
  (let* ((username (username self))
         (password (password self))
         (valid-user (validate-user username password)))
    (if valid-user
        (if (disabled valid-user)
            (setf (message self) "Sorry! Your account has been disabled. 
Please contact site administrator.")
            (progn              
              (setf (get-session-value 'user) valid-user)
	      ;(save-login-time username)
              (call 'redirect-component :target "dashboard")))
        (setf (message self) "Your login information was incorrect!"))))

(defun disabled (username)
  nil)

(defentry-point "login" (:application *aspiwork-pdm-application*)
    ()
    (if (= 0 (car (clsql:select [count [*]] :from [companies] :flatp t)))	
	(call 'get-company)(call 'aspiwork-login))
    (call 'aspiwork-login))

(defun validate-user (username password)
    (let ((user (car (clsql:select 'employee 
				   :flatp t :limit 1 :refresh t
				   :where [= [username] username]))))
      (when (and user (string= (password user) password))
        username)))

