;;;; This file is part of Aspiwork Project Data Management Server
;;;; Name:          resource-category.lisp
;;;; Purpose:       
;;;; Programmer:    Aashish R Lokhande
;;;; Copyright (C) 2010,2011 Aashish R Lokhande
;;;; All rights reserved.
;;;;
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions
;;;; are met:
;;;; 1. Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 2. Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;; 3. Neither the name of the author nor the names of its contributors
;;;;    may be used to endorse or promote products derived from this software
;;;;    without specific prior written permission.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
;;;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
;;;; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
;;;; OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;;;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
;;;; LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
;;;; OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
;;;; SUCH DAMAGE.

(defcomponent res-cat-manipulator ()
  ((res-cat :accessor res-cat :initarg :res-cat :backtrack t
	    :initform (make-instance 'resource-category))))

(defcomponent filetype-manipulator ()
  ((filetyp :accessor filetyp :initarg :filetyp :backtrack t
	    :initform (make-instance 'file-type))))

(defcomponent get-res-cat (res-cat-manipulator) 
  ())

(defcomponent get-filetype (filetype-manipulator) 
  ())

(defcomponent show-res-cat ()())

(defcomponent show-filetype ()())

(defcomponent res-cat-page (holder)
  ()
  (:default-initargs :message "Resource Category Page"))

(defmethod render ((self get-res-cat))
  (let ((tres-cat (res-cat self)))
    (<ucw:form 
     :action (insert-res-cat tres-cat)
     :method "post"
     (<:table
      :border 1 :style "margin:auto;"
      (<:tr (<:th "Code")(<:th "Name"))
      (<:tr 
       (<:td 
	(<ucw:input 
	 :type "text" :size "6" :accessor (code tres-cat)))		
       (<:td 
	(<ucw:input 
	 :type "text" :accessor (name tres-cat))))
      (<:tr 
       (<:td :align "center" :colspan 2
	     (<:input :type "submit" :value "Add Resource Category")))))))

(defmethod render ((self get-filetype))
  (let ((tfiletyp (filetyp self)))
    (<ucw:form 
     :action (insert-filetype tfiletyp)
     :method "post"
     (<:table
      :border 1 :style "margin:auto;"
      (<:tr (<:th "Name")(<:th "Extension"))
      (<:tr 
       (<:td 
	(<ucw:input 
	 :type "text" :accessor (name tfiletyp)))		
       (<:td 
	(<ucw:input 
	 :type "text" :size 6 :accessor (extension tfiletyp))))
      (<:tr 
       (<:td :align "center" :colspan 2
	     (<:input :type "submit" :value "Add File-Type")))))))

(defaction insert-res-cat ((tres-cat resource-category))
  (let ((name (sanitize-input (name tres-cat)))
	(code (sanitize-input (code tres-cat))))
    (unless (and (or (string= "" name)(string= "" code))
		 (clsql:select [id] 
			       :from [resource_categories] 
			       :where [= [name] (name tres-cat)]
			       :flatp t))
		 (setf (slot-value tres-cat 'code) code)
		 (setf (slot-value tres-cat 'name) name)
		 (setf (slot-value tres-cat 'dept-id) 0)
		 (insert-update-res-cat tres-cat))))

(defaction insert-filetype ((tfiletyp file-type))
  (let ((name (sanitize-input (name tfiletyp)))
	(extension (sanitize-input (extension tfiletyp ))))
    (unless (and (or (string= "" name) (string= "" extension))
		 (clsql:select [id] 
			       :from [file_types] 
			       :where [= [name] (name tfiletyp)]
			       :flatp t))
		 (setf (slot-value tfiletyp 'extension) extension)
		 (setf (slot-value tfiletyp 'name) name)		 
		 (insert-update-filetype tfiletyp))))

(defmethod render ((self show-res-cat))
  (let* ( (res-cat-list (clsql:select 
			[slot-value 'resource-category 'id]
			[slot-value 'resource-category 'code]
			[slot-value 'resource-category 'name]
			:from [resource_categories]
			:field-names nil)))
    (<:table 
     :border 1 :style "margin:auto;"
     (<:caption "Resource Category List")
     (<:tr (<:th "Code")(<:th "Name"))
     (loop for (id a b ) in res-cat-list
	do (<:tr (<:td (<:ah a))(<:td (<:ah b)))))))

(defmethod render ((self show-filetype))
  (let ((filetype-list (clsql:select 
			[slot-value 'file-type 'id]
			[slot-value 'file-type 'name]
			[slot-value 'file-type 'extension]
			:from [file_types]
			:field-names nil)))
    (<:table 
     :border 1 :style "margin:auto;"
     (<:caption "File Type List")
     (<:tr (<:th "Name") (<:th "Extension"))
     (loop for (id a b ) in filetype-list
	do (<:tr (<:td (<:ah a))(<:td (<:ah b)))))))

(defmethod render :wrapping ((self res-cat-page))
  (call-next-method)  
  (render (make-instance 'show-res-cat))
  (<:p "")
  (render (make-instance 'get-res-cat))
  (<:hr)
  (render (make-instance 'show-filetype))
  (<:p "")
  (render (make-instance 'get-filetype)))

(defun next-res-cat-id ()
  (if (= 0 (car (clsql:select [count [*]] :from [resource-categories] :flatp t)))
      1
      (+ 1 (car 
	    (clsql:select [max [id]] 
			  :from [resource_categories]
			  :flatp t)))))

(defun next-filetype-id ()
  (if (= 0 (car (clsql:select [count [*]] :from [file_types] :flatp t)))
      1
      (+ 1 (car 
	    (clsql:select [max [id]] 
			  :from [file_types]
			  :flatp t)))))

(defun insert-update-res-cat (obj)
  (if (slot-boundp obj 'id)
      (clsql:update-records-from-instance obj)
      (clsql:with-transaction ()	  
	(setf (id obj) (next-res-cat-id))
	(clsql:update-records-from-instance obj))))

(defun insert-update-filetype (obj)
  (if (slot-boundp obj 'id)
      (clsql:update-records-from-instance obj)
      (clsql:with-transaction ()	  
	(setf (id obj) (next-filetype-id))
	(clsql:update-records-from-instance obj))))

(defun res-cat-tlist ();table
  (clsql:select 
   [slot-value 'resource-category 'id]
   [slot-value 'resource-category 'code]
   [slot-value 'resource-category 'name]
   :from [resource_categories]
   :field-names nil))

(defun res-cat-ddlist ();drop-down-list
  (clsql:select 
   [slot-value 'resource-category 'id]
   [slot-value 'resource-category 'name]
   :from [resource_categories]
   :field-names nil))

(defun res-catlist-for-user (user-id);restricting drop-down-list
  (let* ((des-id (car 
		  (clsql:select [desig-id]
				:from [employees]
				:where 
				[= [slot-value 'employee 'id] user-id]
				:flatp t
				:field-names nil))))
  (clsql:select 
   [slot-value 'resource-category 'id]
   [slot-value 'resource-category 'name]
   :from '([resource_categories][permissions])
   :where
   [and
   [= [slot-value 'permission 'desig-id] des-id]
   [= [slot-value 'resource-category 'id] [slot-value 'permission 'category-id]]
   ]
   :field-names nil)))

(defun res-cat-from-id (res-cat-id);object
  (car (clsql:select 'resource-category 
		     :where 
		     [= [slot-value 'resource-category 'id] res-cat-id]
		     :flatp t)))

(defun res-cat-name-from-id (res-cat-id)
  (car (clsql:select [slot-value 'resource-category 'name]
		:from [resource_categories]
		:where [= [slot-value 'resource-category 'id] res-cat-id]
		:field-names nil
		:flatp t)))

(defun res-cat-code-from-id (res-cat-id);name-string
  (car (clsql:select [slot-value 'resource-category 'code]
		:from [resource_categories]
		:where [= [slot-value 'resource-category 'id] res-cat-id]
		:field-names nil
		:flatp t)))


;;;; *******************************************************
;;;; Name:          resource-categories.lisp
;;;; Purpose:       
;;;; Programmer:    Aashish R Lokhande
;;;; Date Started:  April 2010
;;;; This file is part of AspiworkPDM
;;;; Copyright (C) 2010 by Aashish R Lokhande
;;;; ********************************************************