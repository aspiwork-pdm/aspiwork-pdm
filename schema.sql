-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.0.51a-24+lenny4


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema aspiworkpdm
--

CREATE DATABASE IF NOT EXISTS aspiworkpdm;
USE aspiworkpdm;

DROP TABLE IF EXISTS `aspiworkpdm`.`addresses`;
CREATE TABLE  `aspiworkpdm`.`addresses` (
  `LINE1` varchar(50) default NULL,
  `LINE2` varchar(50) default NULL,
  `STATE` varchar(30) default NULL,
  `PINCODE` varchar(6) default NULL,
  `PHONE` varchar(60) default NULL,
  `MOBILE` varchar(15) default NULL,
  `FAX` varchar(15) default NULL,
  `EMAIL` varchar(50) default NULL,
  `WEBSITE` varchar(50) default NULL,
  `ID` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `aspiworkpdm`.`agencies`;
CREATE TABLE  `aspiworkpdm`.`agencies` (
  `CODE` varchar(6) default NULL,
  `NAME` varchar(100) default NULL,
  `INFO` varchar(200) default NULL,
  `ADDRESS_ID` int(11) default NULL,
  `ID` int(11) NOT NULL auto_increment,
  `CREATER_ID` int(11) default NULL,
  `CREATED_DATE` datetime default NULL,
  `MODIFIER_ID` int(11) default NULL,
  `MODIFIED_DATE` datetime default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `aspiworkpdm`.`companies`;
CREATE TABLE  `aspiworkpdm`.`companies` (
  `CODE` varchar(6) default NULL,
  `NAME` varchar(100) default NULL,
  `EXTENSION` varchar(20) default NULL,
  `LOCALIP` varchar(100) default NULL,
  `FSUSER` varchar(50) default NULL,
  `FOLDER` varchar(100) default NULL,
  `ADDRESS_ID` int(11) default NULL,
  `ID` int(11) NOT NULL auto_increment,
  `CREATER_ID` int(11) default NULL,
  `CREATED_DATE` datetime default NULL,
  `MODIFIER_ID` int(11) default NULL,
  `MODIFIED_DATE` datetime default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `aspiworkpdm`.`contact_persons`;
CREATE TABLE  `aspiworkpdm`.`contact_persons` (
  `DESIGNATION` varchar(30) default NULL,
  `INFO` varchar(100) default NULL,
  `PERSON_ID` int(11) NOT NULL,
  `PROJECT_ID` int(11) default NULL,
  `CUSTOMER_ID` int(11) default NULL,
  `AGENCY_ID` int(11) default NULL,
  `ID` int(11) NOT NULL auto_increment,
  `CREATER_ID` int(11) default NULL,
  `CREATED_DATE` datetime default NULL,
  `MODIFIER_ID` int(11) default NULL,
  `MODIFIED_DATE` datetime default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `aspiworkpdm`.`current_files`;
CREATE TABLE  `aspiworkpdm`.`current_files` (
  `RESOURCE_ID` int(11) default NULL,
  `ACCESSOR_ID` int(11) default NULL,
  `ACCESSED_DATE` datetime default NULL,
  `CENTRAL_LOCATION` varchar(200) default NULL,
  `FETCH_LOCATION` varchar(200) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `aspiworkpdm`.`customers`;
CREATE TABLE  `aspiworkpdm`.`customers` (
  `CODE` varchar(6) default NULL,
  `NAME` varchar(100) default NULL,
  `INFO` varchar(200) default NULL,
  `ADDRESS_ID` int(11) default NULL,
  `ID` int(11) NOT NULL auto_increment,
  `CREATER_ID` int(11) default NULL,
  `CREATED_DATE` datetime default NULL,
  `MODIFIER_ID` int(11) default NULL,
  `MODIFIED_DATE` datetime default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `aspiworkpdm`.`departments`;
CREATE TABLE  `aspiworkpdm`.`departments` (
  `CODE` varchar(6) default NULL,
  `NAME` varchar(30) default NULL,
  `HEAD_ID` int(11) default NULL,
  `ID` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `aspiworkpdm`.`designations`;
CREATE TABLE  `aspiworkpdm`.`designations` (
  `CODE` varchar(6) default NULL,
  `TITLE` varchar(30) default NULL,
  `DIRECTORAL` int(1) default NULL,
  `DEPARTMENTAL` int(1) default NULL,
  `MANAGEMENT` int(1) default NULL,
  `LEGAL` int(1) default NULL,
  `FINANCIAL` int(1) default NULL,
  `PR` int(1) default NULL,
  `HR` int(1) default NULL,
  `EXPERT` int(1) default NULL,
  `OPERATION` int(1) default NULL,
  `GENERAL` int(1) default NULL,
  `ID` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `aspiworkpdm`.`employees`;
CREATE TABLE  `aspiworkpdm`.`employees` (
  `CODE` varchar(6) default NULL,
  `USERNAME` varchar(30) default NULL,
  `PASSWORD` varchar(20) default NULL,
  `LOCALIP` varchar(100) default NULL,
  `PCUSER` varchar(50) default NULL,
  `PCPWD` varchar(50) default NULL,
  `PCSHARE` varchar(100) default NULL,
  `FOLDER` varchar(100) default NULL,
  `ACTIVE` int(1) default NULL,
  `LAST_LOGIN` datetime default NULL,
  `LAST_LOGOUT` datetime default NULL,
  `LEVEL` int(11) default NULL,
  `JOIN_DATE` date default NULL,
  `MANAGER_ID` int(11) default NULL,
  `QUALIFICATION` varchar(20) default NULL,
  `EXPERIENCE` int(11) default NULL,
  `EXPERTISE` varchar(100) default NULL,
  `INFO` varchar(200) default NULL,
  `PERSON_ID` int(11) NOT NULL,
  `DEPT_ID` int(11) NOT NULL,
  `DESIG_ID` int(11) NOT NULL,
  `ID` int(11) NOT NULL auto_increment,
  `CREATER_ID` int(11) default NULL,
  `CREATED_DATE` datetime default NULL,
  `MODIFIER_ID` int(11) default NULL,
  `MODIFIED_DATE` datetime default NULL,
  PRIMARY KEY  (`ID`),
  UNIQUE KEY `USERNAME` (`USERNAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `aspiworkpdm`.`file_types`;
CREATE TABLE  `aspiworkpdm`.`file_types` (
  `NAME` varchar(30) default NULL,
  `EXTENSION` varchar(20) default NULL,
  `ID` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `aspiworkpdm`.`permissions`;
CREATE TABLE  `aspiworkpdm`.`permissions` (
  `DESIG_ID` int(11) NOT NULL,
  `CATEGORY_ID` int(11) NOT NULL,
  `ID` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `aspiworkpdm`.`persons`;
CREATE TABLE  `aspiworkpdm`.`persons` (
  `TITLE` varchar(3) default NULL,
  `FIRST_NAME` varchar(30) default NULL,
  `MIDDLE_NAME` varchar(30) default NULL,
  `LAST_NAME` varchar(30) default NULL,
  `GENDER` varchar(1) default NULL,
  `BIRTH_DATE` date default NULL,
  `MARRIED` int(1) default NULL,
  `ADDRESS_ID` int(11) default NULL,
  `ID` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `aspiworkpdm`.`project_agencies`;
CREATE TABLE  `aspiworkpdm`.`project_agencies` (
  `PROJECT_ID` int(11) default NULL,
  `AGENCY_ID` int(11) default NULL,
  `ID` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `aspiworkpdm`.`project_sections`;
CREATE TABLE  `aspiworkpdm`.`project_sections` (
  `CODE` varchar(6) default NULL,
  `NAME` varchar(30) default NULL,
  `INFO` varchar(200) default NULL,
  `PROJECT_ID` int(11) default NULL,
  `ID` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `aspiworkpdm`.`projects`;
CREATE TABLE  `aspiworkpdm`.`projects` (
  `CODE` varchar(6) default NULL,
  `NAME` varchar(50) default NULL,
  `MANAGER_ID` int(11) default NULL,
  `ACTIVE` int(1) default NULL,
  `START_DATE` date default NULL,
  `FINISH_DATE` date default NULL,
  `INFO` varchar(200) default NULL,
  `ADDRESS_ID` int(11) default NULL,
  `DEPT_ID` int(11) NOT NULL,
  `CUSTOMER_ID` int(11) default NULL,
  `ID` int(11) NOT NULL auto_increment,
  `CREATER_ID` int(11) default NULL,
  `CREATED_DATE` datetime default NULL,
  `MODIFIER_ID` int(11) default NULL,
  `MODIFIED_DATE` datetime default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `aspiworkpdm`.`resource_categories`;
CREATE TABLE  `aspiworkpdm`.`resource_categories` (
  `CODE` varchar(6) default NULL,
  `NAME` varchar(30) default NULL,
  `DEPT_ID` int(11) NOT NULL,
  `ID` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `aspiworkpdm`.`resources`;
CREATE TABLE  `aspiworkpdm`.`resources` (
  `CODE` varchar(10) default NULL,
  `NAME` varchar(100) default NULL,
  `REF_ID` int(11) default NULL,
  `CURRENT` varchar(100) default NULL,
  `FOLDER` varchar(100) default NULL,
  `IN_USE` int(1) default NULL,
  `LOCKED` int(1) default NULL,
  `VERSION` int(11) default NULL,
  `REVISION` int(11) default NULL,
  `SAVE_COUNT` int(11) default NULL,
  `INFO` varchar(200) default NULL,
  `COMMENT` varchar(200) default NULL,
  `INIT_COMMENT` varchar(200) default NULL,
  `LAST_COMMENT` varchar(200) default NULL,
  `PROJECT_ID` int(11) default NULL,
  `SECTION_ID` int(11) default NULL,
  `CATEGORY_ID` int(11) NOT NULL,
  `TYPE_ID` int(11) default NULL,
  `ID` int(11) NOT NULL auto_increment,
  `CREATER_ID` int(11) default NULL,
  `CREATED_DATE` datetime default NULL,
  `MODIFIER_ID` int(11) default NULL,
  `MODIFIED_DATE` datetime default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `aspiworkpdm`.`teams`;
CREATE TABLE  `aspiworkpdm`.`teams` (
  `MEMBER_ID` int(11) default NULL,
  `PROJECT_ID` int(11) default NULL,
  `ID` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
