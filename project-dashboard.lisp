;;;; This file is part of Aspiwork Project Data Management Server
;;;; Name:          project-dashboard.lisp
;;;; Purpose:       
;;;; Programmer:    Aashish R Lokhande
;;;; Copyright (C) 2010,2011 Aashish R Lokhande
;;;; All rights reserved.
;;;;
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions
;;;; are met:
;;;; 1. Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 2. Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;; 3. Neither the name of the author nor the names of its contributors
;;;;    may be used to endorse or promote products derived from this software
;;;;    without specific prior written permission.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
;;;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
;;;; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
;;;; OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;;;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
;;;; LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
;;;; OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
;;;; SUCH DAMAGE.

(defcomponent view-project-files (project-component holder)
  ((sec-id :accessor sec-id :initform 0 :initarg :sec-id)
   (cat-id :accessor cat-id :initform 0 :initarg :cat-id)
   (ft-id :accessor ft-id :initform 0 :initarg :ft-id))
  (:default-initargs :message "Project File Browser"))

(defmethod render :wrapping ((self view-project-files))
  (call-next-method)
  (let* ((user $user)
	 (prj-id (project-id self))
	 (sec-id (sec-id self))
	 (cat-id (cat-id self))
	 (ft-id (ft-id self))
	 (user-id (id user))
	 (resfiles (if (= 0 sec-id cat-id ft-id)
		       nil
		       (resfiles-basic
			prj-id cat-id ft-id sec-id)))
	 (file-id 0))
    (render (make-instance 'project-links :project-id prj-id))
    (<:hr)
    (<:ah (project-name-from-id prj-id))
    (<:hr)
    (<ucw:form 
     :action (view-prj-files prj-id cat-id ft-id sec-id)
     :method "post" 
     (<:table 
      :border 1 :style "margin:auto;"
      (<:tr (<:td :align "center"
		  (<ucw:select :accessor sec-id
			       (<ucw:option :value 0 "Section")
			       (loop for (id b) in (seclist-from-prjid prj-id)
				  do (<ucw:option :value id (<:ah b)))) 
		  (<:ah " ")
		  (<ucw:select :accessor cat-id
			       (<ucw:option :value 0 "Category")
			       (loop for (id b) in (res-catlist-for-user user-id)
				  do (<ucw:option :value id (<:ah b))))
		  (<:ah " ") 
		  (<ucw:select :accessor ft-id
			       (<ucw:option :value 0 "Type")
			       (loop for (id b) in (filetype-ddlist)
				  do (<ucw:option :value id (<:ah b))))))
      (<:tr (<:td :align "center" :colspan 3
		  (<:input :type "submit" :value "Get List")))))
    (<:p )
    (<ucw:form 
     :action (view-file-revs prj-id file-id)
     :method "post"  
     (<:table 
      :border 1 :style "margin:auto;"
      (<:tr (<:th "Name") (<:th "Select"))
      (loop for (id nm) in resfiles
	 do (<:tr (<:td (<:ah nm))
		  (<:td (<ucw:input :type "radio" :name "selected"
				    :accessor file-id :value id))))
      (<:tr 
       (<:td :align "center" :colspan 2
       (<:input :type "submit" :value "View Revisions")))))))

(defaction view-prj-files (prj-id cat-id ft-id sec-id)
  (call-component 
   $holder (make-instance 'view-project-files
			  :project-id prj-id
			  :sec-id sec-id
			  :cat-id cat-id
			  :ft-id ft-id)))

(defaction view-file-revs (prj-id file-id)
  (call-component 
   $holder (make-instance 'view-file-revisions
			  :project-id prj-id
			  :file-id (qp-to-int file-id))))

(defcomponent project-summary (project-component holder)
  ()(:default-initargs :message "Project Summary"))

(defmethod render :wrapping ((self project-summary))
  (call-next-method)
  (let* ((prj-id (project-id self)))
    (render (make-instance 'project-links :project-id prj-id))
    (<:hr)
    (<:ah (project-name-from-id prj-id))
    (render (make-instance 'show-project 
			   :project (project-from-id prj-id)))))

(defcomponent view-file-revisions (project-component holder)
  ((file-id :accessor file-id :initarg :file-id))
  (:default-initargs :message "File Revisions"))

(defmethod render :wrapping ((self view-file-revisions))
  (call-next-method)
  (let* ((user $user)
	 (prj-id (project-id self))
	 (rev-list (resfiles-revisions prj-id (file-id self)))
	 (f-id 0))
    (render (make-instance 'project-links :project-id prj-id))    
    (<:hr)
    (<:ah (project-name-from-id prj-id))
    (<:hr)    
    (<ucw:form 
     :action (fetch-file self user f-id)
     :method "post"
     (<:table 
      :border 1 :style "margin:auto;"
      (<:tr (<:th  "Name") (<:th "Save Count")
	    (<:th "Last-modified")(<:th "Select"))
      (loop for (id nm sc md) in rev-list
	 do (<:tr (<:td (<:ah nm)) (<:td (<:ah sc))
		  (<:td (<:ah md))
		  (<:td (<ucw:input :type "radio" :name "selected"
				    :accessor f-id :value id))))
      (<:tr 
       (<:td :align "center" :colspan 4
	     (<:input :type "submit" :value "Fetch to my PC")))))
     (<:p "")
     (<ucw:a :action (answer-component self nil) "Go Back.")))

(defun resfiles-basic (prj-id cat-id type-id sec-id)
  (clsql:select
   [slot-value 'resource 'id]
   [slot-value 'resource 'name]   
   :from [|resources|]
   :where
   [and
   [=[slot-value 'resource 'project-id] prj-id]
   [=[slot-value 'resource 'ref-id] 0]
   [=[slot-value 'resource 'category-id] cat-id]
   [=[slot-value 'resource 'type-id] type-id]
   [=[slot-value 'resource 'section-id] sec-id]
   ]   
   :field-names nil))

(defun seclist-from-prjid (prj-id);for drop down
  (clsql:select
   [slot-value 'project-section 'id]   
   [slot-value 'project-section 'name]
   :from [project_sections]
   :where [=[slot-value 'project-section 'project-id] prj-id]
   :field-names nil))

(defun filetype-ddlist ();for drop-down-list
  (clsql:select 
   [slot-value 'file-type 'id] 
   [slot-value 'file-type 'name]   
   :from [file-types]
   :field-names nil))

(defun resfiles-revisions (prj-id file-id)
  (clsql:select
   [slot-value 'resource 'id]
   [slot-value 'resource 'current]  
   [slot-value 'resource 'save-count]
   [slot-value 'resource 'modified-date]
   :from [resources]
   :where
   [and
   [=[slot-value 'resource 'project-id] prj-id]
   [or
   [=[slot-value 'resource 'id] file-id]
   [=[slot-value 'resource 'ref-id] file-id]]]
   :field-names nil
   :order-by '(([save_count] :desc))))



