;;;; This file is part of Aspiwork Project Data Management Server
;;;; Name:          user-dashboard.lisp
;;;; Purpose:       to access permitted projects and current files
;;;; Programmer:    Aashish R Lokhande
;;;; Copyright (C) 2010,2011 Aashish R Lokhande
;;;; All rights reserved.
;;;;
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions
;;;; are met:
;;;; 1. Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 2. Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;; 3. Neither the name of the author nor the names of its contributors
;;;;    may be used to endorse or promote products derived from this software
;;;;    without specific prior written permission.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
;;;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
;;;; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
;;;; OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;;;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
;;;; LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
;;;; OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
;;;; SUCH DAMAGE.

(defcomponent user-summary (holder)
  ()
  (:default-initargs :message "My Summary"))

(defmethod render :wrapping ((self user-summary))
  (call-next-method)
  (let* ((user $user)
	 (temp (emp-from-id (id user))))    
    (render (make-instance 'show-employee 
			   :employee temp))
    (<:hr)
    (<ucw:a 
     :action (call-component 
	      $holder (make-instance 'change-password-page))
     "Change Password")))

(defcomponent my-projects (holder)
  ()
  (:default-initargs :message "My Projects"))

(defmethod render :wrapping ((self my-projects))
  (call-next-method)
  (let* ((user $user)
	 (prj-list (user-projects (id user)))
	 (prjid 0))
    (<ucw:form 
     :action (goto-project-dashboard user prjid)
     (<:table 
      :border 1 :style "margin:auto;"
      (<:tr (<:th "Name") (<:th "Select"))
      (loop for (id nm) in prj-list
	 do (<:tr (<:td (<:ah nm))
		  (<:td (<ucw:input :type "radio" :name "selected"
				    :accessor prjid :value id))))
      (<:tr 
       (<:td :align "center" :colspan 2
	     (<:input :type "submit" :value "View Project")))))))

(defaction goto-project-dashboard ((user employee) prjid)
  (call-component 
   $holder
   (make-instance 
    'project-summary
    :project-id prjid)))

(defcomponent my-files (holder)
  ()
  (:default-initargs :message "My Working Files"))

(defmethod render :wrapping ((self my-files))
  (call-next-method)
  (let* ((user $user)
	 (file-list (current-user-files (id user)))
	 (file-id 0) (keep "") (comment ""))
    (<ucw:form 
     :action (update-file self user file-id keep comment)
     (<:table 
      :border 1 :style "margin:auto;"
      (<:tr (<:th "File") (<:th "Select"))
      (loop for (id nm) in file-list
	 do (<:tr (<:td (<:ah nm))
		  (<:td (<ucw:input :type "radio" :name "selected"
				    :accessor file-id :value id)))))
     (<:p " ")
     (<:table
      :border 1 :style "margin:auto;"
      (<:tr (<:td (<:ah "Description of changes:"))
	    (<:td (<ucw:input :type "text" :accessor comment)))
      (<:tr (<:td (<:ah "Keep File in My Working List:"))
	    (<:td (<ucw:input 
		   :type "checkbox" :accessor keep :value "1"))))
     (<:table
      :style "margin:auto;"
      (<:tr (<:td :align "center"
	     (<:submit :value "Update")(<:ah " ")
	     (<ucw:submit 
	      :action (revise-file self user file-id 1 keep comment)
	      :value "Save as Revision")(<:ah " ")
	      (<ucw:submit
	       :action (revise-file self user file-id 2 keep comment)
	       :value "Save as New Version"))))) ))

(defcomponent change-password-page (holder)
  ((oldpwd :accessor oldpwd :initform nil)
   (newpwd :accessor newpwd :initform nil)
   (renewpwd :accessor renewpwd :initform nil))
  (:default-initargs :message "Change Password Page"))

(defmethod render :wrapping ((self change-password-page))
  (call-next-method)
  (let* ((user $user))
    (<ucw:form 
     :action (change-user-password self old new renew)
     (<:table
      :border 1
      (<:tr (<:td "Old Password: ") 
	    (<:td (<ucw:input :type "password"
			      :accessor (oldpwd self))))
      (<:tr (<:td "New Password: ") 
	    (<:td (<ucw:input :type "password"
			      :accessor (newpwd self))))
      (<:tr (<:td "Re-enter New Password: ") 
	    (<:td (<ucw:input :type "password"
			      :accessor (renewpwd self))))
      (<:tr (<:td :align "left" :colspan 2
			   (<:button :type "submit" "Reset Password"))))
     (<:hr)
     (<:ah "You shall be redirected to login page to login with new password"))))

(defaction change-user-password ((self change-password-page)
				 oldpwd newpwd renewpwd)
  (let* ((user $user)
	 (existing (clsql:select
		    [password]
		    :from [employees] 
		    :flatp t :limit 1
		    :where [= [username] (username user)])))
    (if (and (string= existing oldpwd)
	     (string= newpwd renewpwd))
	(progn
	  (clsql:update-records 
	   [|employees|]
	   :av-pairs `((password ,renewpwd))
	   :where [= [username] (username user)])
	  (logout))
	(setf (message self) "Mismatch. Try again!"))))

(defun user-projects (user-id)
  (clsql:select
   [slot-value 'project 'id]
   [slot-value 'project 'name]   
   :from '([projects][teams])
   :where
   [and
   [=[slot-value 'team 'member-id] user-id]
   [=[slot-value 'team 'project-id][slot-value 'project 'id]]
   ]
   :field-names nil ))

(defun current-user-files (user-id)
  (clsql:select 
   [slot-value 'resource 'id]
   [slot-value 'resource 'current]
   :from '([current_files] [resources])
   :where [and 
   [= [slot-value 'current-file 'accessor-id] user-id]
   [= [slot-value 'current-file 'resource-id][slot-value 'resource 'id]]]
   :field-names nil))
