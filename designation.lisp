;;;; This file is part of Aspiwork Project Data Management Server
;;;; Name:          designation.lisp
;;;; Purpose:       
;;;; Programmer:    Aashish R Lokhande
;;;; Copyright (C) 2010,2011 Aashish R Lokhande
;;;; All rights reserved.
;;;;
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions
;;;; are met:
;;;; 1. Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 2. Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;; 3. Neither the name of the author nor the names of its contributors
;;;;    may be used to endorse or promote products derived from this software
;;;;    without specific prior written permission.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
;;;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
;;;; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
;;;; OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;;;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
;;;; LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
;;;; OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
;;;; SUCH DAMAGE.

(defcomponent desig-manipulator ()
  ((desig :accessor desig :initarg :desig :backtrack t
	  :initform (make-instance 'designation))))

(defcomponent get-desig (desig-manipulator) 
  ())

(defcomponent show-desigs ()())

(defcomponent designations-page (holder)
  ()
  (:default-initargs :message "Designation Editor"))

(defmethod render ((self get-desig))
  (let ((user $user)
		(tdesig (desig self)))
    (<ucw:form 
     :action (insert-desig tdesig) 
     :method "post"
     (<:table
      :border 1 :style "margin:auto;"      
      (<:tr (<:th "Code")(<:th "Title")(<:th "DIR")
	    (<:th "DPT")(<:th "MNG")(<:th "LGL")
	    (<:th "FIN")(<:th "P.R.")(<:th "H.R.")
	    (<:th "EXPT")(<:th "OPR")(<:th "GEN"))
      (<:tr 
       (<:td 
	(<ucw:input 
	 :type "text" :size "6" :accessor (code tdesig)))		
       (<:td 
	(<ucw:input 
	 :type "text" :accessor (title tdesig)))       
       (<:td 
	(<ucw:input 
	 :type "checkbox" :accessor (directoral tdesig) :value 1
	 :checked (when (= 1 (directoral tdesig)))))		
       (<:td 
	(<ucw:input 
	 :type "checkbox" :accessor (departmental tdesig) :value 1
	 :checked (when (= 1 (departmental tdesig)))))		
       (<:td 
	(<ucw:input 
	 :type "checkbox" :accessor (management tdesig) :value 1
	 :checked (when (= 1 (management tdesig)))))		
       (<:td 
	(<ucw:input 
	 :type "checkbox" :accessor (legal tdesig) :value 1
	 :checked (when (= 1 (legal tdesig)))))		
       (<:td 
	(<ucw:input 
	 :type "checkbox" :accessor (financial tdesig) :value 1
	 :checked (when (= 1 (financial tdesig)))))		
       (<:td 
	(<ucw:input 
	 :type "checkbox" :accessor (pr tdesig) :value 1
	 :checked (when (= 1 (pr tdesig)))))		
       (<:td 
	(<ucw:input 
	 :type "checkbox" :accessor (hr tdesig) :value 1
	 :checked (when (= 1 (hr tdesig)))))		
       (<:td 
	(<ucw:input 
	 :type "checkbox" :accessor (expert tdesig) :value 1
	 :checked (when (= 1 (expert tdesig)))))		
       (<:td 
	(<ucw:input 
	 :type "checkbox" :accessor (operation tdesig) :value 1
	 :checked (when (= 1 (operation tdesig)))))		
       (<:td 
	(<ucw:input 
	 :type "checkbox" :accessor (general tdesig) :value 1
	 :checked (when (= 1 (general tdesig))))))
      (<:tr (<:td :align "right" :colspan 12		
		  (<:input :type "submit" :value "Add Designation")))))))

(defaction insert-desig ((tdesig designation))
  (let ((name (sanitize-input (title tdesig))))    
    (unless (and (string= "" name) 
		 (clsql:select [id] 
			       :from [designations] 
			       :where [= [title] (title tdesig)]
			       :flatp t))
      (setf (slot-value tdesig 'directoral) (qp-to-int (directoral tdesig)))
      (setf (slot-value tdesig 'departmental) (qp-to-int (departmental tdesig)))
      (setf (slot-value tdesig 'management) (qp-to-int (management tdesig)))
      (setf (slot-value tdesig 'legal) (qp-to-int (legal tdesig)))
      (setf (slot-value tdesig 'financial) (qp-to-int (financial tdesig)))
      (setf (slot-value tdesig 'pr) (qp-to-int (pr tdesig)))
      (setf (slot-value tdesig 'hr) (qp-to-int (hr tdesig)))
      (setf (slot-value tdesig 'expert) (qp-to-int (expert tdesig)))
      (setf (slot-value tdesig 'operation) (qp-to-int (operation tdesig)))
      (setf (slot-value tdesig 'general) (qp-to-int (general tdesig))) 
      (insert-update-desig tdesig))))

(defmethod render ((self show-desigs))
  (let* ((user $user)
	 (desig-list (clsql:select 
		      [slot-value 'designation 'id]
		      [slot-value 'designation 'code]
		      [slot-value 'designation 'title]   
		      [slot-value 'designation 'directoral]
		      [slot-value 'designation 'departmental]
		      [slot-value 'designation 'management]
		      [slot-value 'designation 'legal]
		      [slot-value 'designation 'financial]
		      [slot-value 'designation 'pr]
		      [slot-value 'designation 'hr]
		      [slot-value 'designation 'expert]
		      [slot-value 'designation 'operation]
		      [slot-value 'designation 'general]
		      :from [designations]))
	 (des-id 0))
    (<ucw:form 
     :action (goto-permissions-page des-id)
     (<:table 
      :border 1 :style "margin:auto;"
      (<:caption "Designation List with Authority Level")
      (<:tr (<:th "Code")(<:th "Title")(<:th "DIR")
	    (<:th "DPT")(<:th "MNG")(<:th "LGL")
	    (<:th "FIN")(<:th "P.R.")(<:th "H.R.")
	    (<:th "EXPT")(<:th "OPR")(<:th "GEN")(<:th "Select"))
      (loop for (id a b c d e f g h i j k l) in desig-list
	 do (<:tr (<:td (<:ah a))(<:td (<:ah b))(<:td (<:ah c))
		  (<:td (<:ah d))(<:td (<:ah e))(<:td (<:ah f))
		  (<:td (<:ah g))(<:td (<:ah h))(<:td (<:ah i))
		  (<:td (<:ah j))(<:td (<:ah k))(<:td (<:ah l))
		  (<:td (<ucw:input :type "radio" :name "selected"
				    :accessor des-id :value id))))
      (<:tr (<:td :align "right" :colspan 13
		  (<:input :type "submit" :value "Edit Permissions")))))))

(defaction goto-permissions-page (des-id)
  (call-component $holder (make-instance 
			   'permission-page :desig-id des-id)))

(defmethod render :wrapping ((self designations-page))
  (call-next-method)  
  (render (make-instance 'show-desigs))
  (<:hr )
  (render (make-instance 'get-desig)))

(defun next-desig-id ()
  (if (= 0 (car (clsql:select [count [*]] :from [designations] :flatp t)))
      1
      (+ 1 (car 
	    (clsql:select [max [id]] 
			  :from [designations]
			  :flatp t)))))

(defun insert-update-desig (obj)
  (if (slot-boundp obj 'id)
      (clsql:update-records-from-instance obj)
      (clsql:with-transaction ()	  
	(setf (id obj) (next-desig-id))
	(clsql:update-records-from-instance obj))))

(defun desig-tlist ();for table
  (clsql:select 
   [slot-value 'designation 'id]
   [slot-value 'designation 'code]
   [slot-value 'designation 'title]   
   [slot-value 'designation 'directoral]
   [slot-value 'designation 'departmental]
   [slot-value 'designation 'management]
   [slot-value 'designation 'legal]
   [slot-value 'designation 'financial]
   [slot-value 'designation 'pr]
   [slot-value 'designation 'hr]
   [slot-value 'designation 'expert]
   [slot-value 'designation 'operation]
   [slot-value 'designation 'general]
   :from [designations]
   :field-names nil))

(defun desig-ddlist ();for drop-down-list
  (clsql:select 
   [slot-value 'designation 'id]
   [slot-value 'designation 'title]
   :from [designations]
   :field-names nil))
		

(defun desig-from-id (desig-id);object
  (car (clsql:select 'designation 
		     :where [= [slot-value 'designation 'id] desig-id]
		     :flatp t)))

(defun desig-name-from-id (id)
  (car (clsql:select
	[slot-value 'designation 'title]
	:from [designations]
	:where
	[= [slot-value 'designation 'id] id]
	:field-names nil
	:flatp t)))

(defun management-desigs ()
  (clsql:select 
   [slot-value 'designation 'id]
   :from [designations]
   :where
   [or
   [=[slot-value 'designation 'directoral] 1]
   [=[slot-value 'designation 'departmental] 1]
   [=[slot-value 'designation 'management] 1]]
   :flatp t
   :field-names nil))
