;;;; This file is part of Aspiwork Project Data Management server
;;;; Name:          customer.lisp
;;;; Purpose:       
;;;; Programmer:    Aashish R Lokhande
;;;; Copyright (C) 2010,2011 Aashish R Lokhande
;;;; All rights reserved.
;;;;
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions
;;;; are met:
;;;; 1. Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 2. Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;; 3. Neither the name of the author nor the names of its contributors
;;;;    may be used to endorse or promote products derived from this software
;;;;    without specific prior written permission.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
;;;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
;;;; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
;;;; OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;;;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
;;;; LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
;;;; OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
;;;; SUCH DAMAGE.

(defcomponent customer-manipulator ()
  ((customer :accessor customer :initarg :customer :backtrack t
            :initform (make-instance 'customer))))

(defcomponent show-customer (customer-manipulator)
  ())

(defmethod render ((self show-customer))
  (let ((tcustomer (customer self)))
    (<:p (<:ah "Basic Customer Information"))
    (in-table
     ((<:label "Customer Name")
      (<:ah (name tcustomer)))
     ((<:label "Customer Code")
      (<:ah (code tcustomer)))
     ((<:label "Customer Info")
      (<:ah (info tcustomer))))
    (<:p (<:ah "Address"))
    (render 'show-address 
	    :address (address-from-id (address-id tcustomer)))))

(defcomponent get-customer (customer-manipulator holder)
  ()
  (:default-initargs :message "Customer Editor"))

(defmethod render :wrapping ((self get-customer))
  (call-next-method)
  (let* ((user $user)
	 (tcustomer (customer self))	
	 (taddress (if (slot-boundp tcustomer 'id)
		       (address tcustomer)
		       (make-instance 'address))))
    (<ucw:form :action (update-customer self tcustomer taddress user) 
	       :method "post"
	       (<:p "Basic Customer Information")
	       (in-table
		((<:label "Customer Code")
		 (<ucw:input :type "text" 
			     :accessor (code tcustomer)))
		((<:label "Customer Name")
		 (<ucw:input :type "text" 
			     :accessor (name tcustomer)))		
		((<:label "Customer Info")
		 (<ucw:input :type "text" 
			     :accessor (info tcustomer))))
	       (<:p (<:ah "Address"))
	       (render (make-instance 'get-address :address taddress))
	       (<:table 
		:style "margin:auto;"
		(<:tr 
		 (<:td :align "right"
		       (<:input :type "submit" :value "Submit" )))))))

(defaction update-customer ((self get-customer)(tcustomer customer)
			    (taddress address)(user employee))
  (let ((user $user))
  (if (string= "" (name tcustomer))
      (setf (message self) "Error: Name")  
      (progn
	(insert-update-address taddress)
	(if (slot-boundp tcustomer 'id)
	    (progn
	      (setf (modifier-id tcustomer) (id user))
	      (setf (modified-date tcustomer) (clsql:get-time)))
	    (progn
	      (setf (slot-value tcustomer 'address-id) (id taddress))	      
	      (setf (creater-id tcustomer) (id user))
	      (setf (created-date tcustomer) (clsql:get-time))))
	(insert-update-customer tcustomer)
	(answer)))))

(defaction view-customer (id)
  (call-component $holder (make-instance 
			   'show-customer :customer (customer-from-id id))))

(defaction edit-customer (id)
  (call-component $holder (make-instance 
			   'get-customer :customer (customer-from-id id))))

(defcomponent list-customers (holder)
  ()(:default-initargs :message "Customer List"))

(defmethod render :wrapping ((lc list-customers))
  (call-next-method)
  (let* ((user $user)
	 (cl-list (customer-tlist))
	 (cl-id 0))
    (<ucw:a 
     :action (call-component 
	      $holder (make-instance 'get-customer))
     "New Customer")
    (<:hr)
    (<ucw:form 
     :action (edit-customer cl-id)
     (<:table 
      :border 1 :style "margin:auto;"
      (<:tr (<:th "Code") (<:th "Name") (<:th "Select"))
      (loop for (id a b) in cl-list
	 do (<:tr (<:td (<:ah a)) (<:td (<:ah b))
		  (<:td (<ucw:input :type "radio" :name "selected"
				    :accessor cl-id :value id))))
      (<:tr 
       (<:td :align "right" :colspan 3
	     (<:input :type "submit" :value "Edit")))))))

(defun next-customer-id ()
  (if (= 0 (car (clsql:select [count [*]] :from [customers] :flatp t)))
      1
      (+ 1 (car 
	    (clsql:select [max [id]] 
			  :from [customers]
			  :flatp t)))))

(defun insert-update-customer (obj)
  (if (slot-boundp obj 'id)
      (clsql:update-records-from-instance obj)
      (clsql:with-transaction ()	  
	(setf (id obj) (next-customer-id))
	(clsql:update-records-from-instance obj))))

(defun customer-from-id (cl-id);object
  (clsql:select 'customer   
		:where [=[slot-value 'customer 'id] cl-id]
		:flatp t))

(defun customer-name-from-id (cl-id);name string  
  (clsql:select [slot-value 'customer 'name]
		:from [customers]
		:where [=[slot-value 'customer 'id] cl-id]
		:field-names nil
		:flatp t))

(defun customer-tlist ();table
  (clsql:select 
   [slot-value 'customer 'id] 
   [slot-value 'customer 'code]
   [slot-value 'customer 'name]     
   :from [customers]
   :field-names nil))

(defun customer-ddlist ();for drop-down-list
  (clsql:select 
   [slot-value 'customer 'id] 
   [slot-value 'customer 'name]   
   :from [customers]
   :field-names nil))
