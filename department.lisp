;;;; This file is part of Aspiwork Project Data Management server
;;;; Name:          department.lisp
;;;; Purpose:       
;;;; Programmer:    Aashish R Lokhande
;;;; Copyright (C) 2010,2011 Aashish R Lokhande
;;;; All rights reserved.
;;;;
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions
;;;; are met:
;;;; 1. Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 2. Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;; 3. Neither the name of the author nor the names of its contributors
;;;;    may be used to endorse or promote products derived from this software
;;;;    without specific prior written permission.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
;;;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
;;;; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
;;;; OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;;;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
;;;; LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
;;;; OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
;;;; SUCH DAMAGE.

(defcomponent dept-manipulator ()
  ((dept :accessor dept :initarg :dept :backtrack t
		 :initform (make-instance 'department))))

(defcomponent get-dept (dept-manipulator) ())

(defcomponent show-depts ()())

(defcomponent departments-page (holder)
  ()
  (:default-initargs :message "Department-Editor"))

(defmethod render ((self get-dept))  
  (let ((user $user)
	(tdept (dept self)))
    (<ucw:form 
     :action (insert-dept tdept) 
     :method "post"	
     (<:table 
      :border 1 :style "margin:auto;"
      (<:tr (<:th "Code")(<:th "Name"))
      (<:tr (<:td (<ucw:input :type "text" :size 6 :accessor (code tdept)))
	    (<:td (<ucw:input :type "text" :accessor (name tdept))))
      (<:tr (<:td :align "center" :colspan 2
		  (<:input :type "submit" :value "Add Department")))))))

(defaction insert-dept ((tdept department)) 
  (let ((name (sanitize-input (name tdept))))
    ;check duplicate
    (unless (string= "" name)	
      (setf (slot-value tdept 'name) name)
      (insert-update-dept tdept))))

(defmethod render ((self show-depts))
  (let ((dept-list (clsql:select 
		    [slot-value 'department 'id]
		    [slot-value 'department 'code]
		    [slot-value 'department 'name]	
		    :from [departments]
		    :field-names nil)))
    (<:table 
     :border 1 :style "margin:auto;"
     (<:caption "Department List")
     (<:tr (<:th "Code")(<:th "Name"))
     (loop for (id a b) in dept-list
		do (<:tr (<:td (<:ah a))(<:td (<:ah b)))))))

(defmethod render :wrapping ((self departments-page))
  (call-next-method)
  (render (make-instance 'show-depts))	 
  (<:hr)
  (render (make-instance 'get-dept)))

(defun next-dept-id ()
  (if (= 0 (car (clsql:select [count [*]] :from [departments] :flatp t)))
      1
      (+ 1 (car 
	    (clsql:select [max [id]] 
			  :from [departments]
			  :flatp t)))))

(defun insert-update-dept (obj)
  (if (slot-boundp obj 'id)
      (clsql:update-records-from-instance obj)
      (clsql:with-transaction ()	  
	(setf (id obj) (next-dept-id))
	(clsql:update-records-from-instance obj))))

(defun dept-tlist ();for table
  (clsql:select 
   [slot-value 'department 'id]
   [slot-value 'department 'code]
   [slot-value 'department 'name]  
   :from [departments]
   :field-names nil))  

(defun dept-ddlist ();for drop-down-list
  (clsql:select 
   [slot-value 'department 'id] 
   [slot-value 'department 'name]   
   :from [departments]   
   :field-names nil))
   
(defun dept-from-id (dept-id);object
  (clsql:select 'department 
		:where [= [slot-value 'department 'id] dept-id]
		:flatp t))

(defun dept-name-from-id (id)
  (car (clsql:select
	[slot-value 'department 'name]
	:from [departments]
	:where
	[= [slot-value 'department 'id] id]
	:field-names nil
	:flatp t)))
