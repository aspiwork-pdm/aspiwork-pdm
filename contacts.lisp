;;;; This file is part of Aspiwork Project Data Management server
;;;; Name:          contacts.lisp
;;;; Purpose:       
;;;; Programmer:    Aashish R Lokhande
;;;; Copyright (C) 2010,2011 Aashish R Lokhande
;;;; All rights reserved.
;;;;
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions
;;;; are met:
;;;; 1. Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 2. Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;; 3. Neither the name of the author nor the names of its contributors
;;;;    may be used to endorse or promote products derived from this software
;;;;    without specific prior written permission.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
;;;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
;;;; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
;;;; OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;;;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
;;;; LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
;;;; OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
;;;; SUCH DAMAGE.
(defcomponent contact-manipulator ()
  ((contact :accessor contact :initarg :contact :backtrack t
            :initform (make-instance 'contact-person))))

(defcomponent get-contact (contact-manipulator)
  ())

(defmethod render ((self get-contact))
  (let* ((tcontact (contact self))
	 (tperson ;(if (id tcontact)
		      ;(person-from-id (person-id tcontact))
		      (make-instance 'person))
	 (taddress ;(if (id tperson)
		       ;(address-from-id (address-id tperson))
		       (make-instance 'address))
	 (man-list (managerlist-from-coid (get-session-value 'co-id))))
    (<ucw:form 
     :action (insert-new-contact tcontact tperson taddress)
     :method "post"
;personal details
    (<:p "Personal Details")
    (render (make-instance 'get-person :person tperson))
;address 
    (<:p (<:ah "Address"))
    (render (make-instance 'get-address :address taddress))
;company specific details
    (<:p (<:ah "Working Details"))
    (in-table
     ((<:label "Desigantion")
      (<ucw:input :type "text" 
		  :accessor (designation tcontact )))
     ((<:label "Info")
      (<ucw:input :type "text" 
		  :accessor (info tcontact ))))
    (<:input :type "submit" :value "Submit"))))
