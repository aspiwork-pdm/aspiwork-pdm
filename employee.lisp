;;;; This file is part of Aspiwork Project Data Management Server
;;;; Name:          employee.lisp
;;;; Purpose:       
;;;; Programmer:    Aashish R Lokhande
;;;; Copyright (C) 2010,2011 Aashish R Lokhande
;;;; All rights reserved.
;;;;
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions
;;;; are met:
;;;; 1. Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 2. Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;; 3. Neither the name of the author nor the names of its contributors
;;;;    may be used to endorse or promote products derived from this software
;;;;    without specific prior written permission.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
;;;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
;;;; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
;;;; OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;;;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
;;;; LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
;;;; OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
;;;; SUCH DAMAGE.

(defcomponent employee-manipulator ()
  ((employee :accessor employee :initarg :employee :backtrack t
	     :initform (make-instance 'employee
				      :dept-id 0
				      :manager-id 0
				      :desig-id 0))))

(defcomponent show-employee (employee-manipulator)
  ())

(defmethod render ((self show-employee))
  (let ((temployee (employee self)))	
    (in-table
     ((<:label "Username") (<:ah (username temployee)))
     ((<:label "PC Username") (<:ah (pcuser temployee)))
     ((<:label "PC Local IP") (<:ah (localip temployee)))
     ((<:label "Shared Folder") (<:ah (pcshare temployee)))
     ((<:label "File-store Folder") (<:ah (folder temployee)))
     ((<:label "Department") (<:ah (emp-dept temployee)))
     ((<:label "Designation") (<:ah (emp-desig temployee)))
     ((<:label "Active") (<:ah (active temployee)))
     ((<:label "Join Date") (<:ah (join-date temployee)))
     ((<:label "Reports to") (<:ah (emp-manager temployee)))
     ((<:label "Qualification") (<:ah (qualification temployee)))
     ((<:label "Experience") (<:ah (experience temployee)))
     ((<:label "Expertise") (<:ah (expertise temployee)))
     ((<:label "Info") (<:ah (info temployee))))))

(defcomponent get-employee (employee-manipulator holder)
  ()(:default-initargs :message "Employee-Editor"))

(defmethod render :wrapping ((self get-employee))
  (call-next-method)
  (let* ((user $user)
	 (temployee (employee self))
	 (tperson (if (slot-boundp temployee 'id)
		      (person temployee)
		      (make-instance 'person)))
	 (taddress (if (slot-boundp tperson 'id)
		       (address tperson)
		       (make-instance 'address)))
	 (jdate (join-date temployee))	 
	 (dept-list (dept-ddlist))
	 (desig-list (desig-ddlist))
	 (man-list (manager-ddlist)))
    (<ucw:form 
     :action (update-employee self temployee tperson taddress user) 
     :method "post"
    (<:p "Personal Details")
    (render (make-instance 'get-person :person tperson))
    (<:p "Working Details")
    (multiple-value-bind (jd jm jy)(clsql:decode-date jdate)
      (in-table      
       ((<:label "PC Username") 
	(<ucw:input :type "text" 
		    :accessor  (pcuser temployee)))
       ((<:label "PC Local IP") 
	(<ucw:input :type "text" 
		    :accessor  (localip temployee)))
       ((<:label "Shared Folder") 
	(<ucw:input :type "text" 
		    :accessor  (pcshare temployee)))
       ((<:label "Department")
	(<ucw:select :accessor (dept-id temployee)
		     (<ucw:option :value 0 "Department")
		     (loop for (id b) in dept-list
			do (<ucw:option :value id (<:ah b)))))
       ((<:label "Designation")
	(<ucw:select :accessor (desig-id temployee)
		     (<ucw:option :value 0 "Designation")
		     (loop for (id b) in desig-list
			do (<ucw:option :value id (<:ah b)))))
       ((<:label "Level")
	(<ucw:select :accessor (level temployee)
		     (dotimes (x 5) 
		       (<ucw:option :value (+ 1 x) 
				    (<:ah (+ 1 x))))))
       ((<:label "Join Date")
	"D :" (<ucw:select :accessor jd
			   (dotimes (x 31) 
			     (<ucw:option :value (+ 1 x) 
					  (<:ah (+ 1 x)))))
	" M :" (<ucw:select :accessor jm
			    (dotimes (x 12) 
			      (<ucw:option :value (+ 1 x) 
					   (<:ah (+ 1 x)))))
	" Y :" (<ucw:select :accessor jy
			    (do ((x 1950 (1+ x))) ((>= x 2020)) 
			      (<ucw:option :value x (<:ah x))))
	(<ucw:input :type "hidden"
		    :writer (lambda (val)
			      (declare (ignore val))			      
			      (setf (slot-value temployee 'join-date)
				    (clsql:make-date :day jd :month jm :year jy)))))
	((<:label "Reports to")
	 (<ucw:select :accessor (manager-id temployee)
		      (<ucw:option :value 0 "Manager")
		      (loop for (id b) in man-list
			 do (<ucw:option :value id (<:ah b)))))
	((<:label "Qualification")
	(<ucw:input :type "text" 
		    :accessor (qualification temployee)))
	((<:label "Experience")
	 (<ucw:input :type "text" 
		     :accessor (experience temployee)))
	((<:label "Expertise")
	 (<ucw:input :type "text" 
		     :accessor (expertise temployee)))
	((<:label "Info")
	 (<ucw:input :type "text" 
		     :accessor (info temployee))))
       (<:p "Address")
       (render (make-instance 'get-address :address taddress))
       (<:table 
	:style "margin:auto;"
	(<:tr 
	 (<:td :align "center"
	       (<:input :type "submit" :value "Submit" ))))))))
    
(defaction update-employee ((self get-employee)(temployee employee) 
			    (tperson person)(taddress address)(user employee))
  (let* ((per-errors (validate-person tperson))
	 (emp-errors (validate-employee temployee))	 
	 (ad-errors (validate-address taddress))
	 (all-errors (concatenate 'string per-errors emp-errors ad-errors)))
    (if (string= "" all-errors)
	(progn
	  (insert-update-address taddress)
	  (if (slot-boundp temployee 'id)
	      (progn
		(insert-update-person tperson)
		(setf (slot-value temployee 'modifier-id) (id user))
		(setf (slot-value temployee 'modified-date) (clsql:get-time)))
	      (progn
		(setf (slot-value tperson 'address-id) (id taddress))
		(insert-update-person tperson)
		(setf (slot-value temployee 'person-id) (id tperson))
		(setf (slot-value temployee 'username) (create-username
							(first-name tperson)
							(last-name tperson)))
		(setf (slot-value temployee 'folder) (create-user-folder
						      (username temployee)))
		(setf (slot-value temployee 'password) 
		      (car (clsql:select [extension] :from [companies] 
					 :where [= [id] 1] :flatp t)))
		(setf (slot-value temployee 'creater-id) (id user))
		(setf (slot-value temployee 'created-date) (clsql:get-time))))
	  (insert-update-employee temployee)
	  (answer))
	(setf (message self) (concatenate 'string "Errors: " all-errors)))))
      
(defgeneric validate-employee (employee)(:documentation ""))

(defmethod validate-employee ((temployee employee))
  (let* ((dp "") (ds ""))
    (progn      
      (setf (slot-value temployee 'dept-id) (qp-to-int (dept-id temployee)))
      (setf (slot-value temployee 'desig-id) (qp-to-int (desig-id temployee)))
      (setf (slot-value temployee 'manager-id) (qp-to-int (manager-id temployee)))
      (setf (slot-value temployee 'level) (qp-to-int (level temployee)))
      (setf (slot-value temployee 'experience) (qp-to-int (experience temployee)))
      (if (= 0 (dept-id temployee))
	  (setf dp "Department "))
      (if (= 0 (desig-id temployee))
	  (setf ds "Designation ")))
    (concatenate 'string dp ds)))

(defcomponent list-employees (holder)
  ()
  (:default-initargs :message "Employee List"))

(defmethod render :wrapping ((lc list-employees))
  (call-next-method)
  (let* ((user $user)
	 (emp-list (employee-tlist))
	 (empid 0))
    (<ucw:a 
     :action (call-component 
	      $holder (make-instance 'get-employee))
     "New Employee")
    (<:hr)
    (<ucw:form 
     :action (edit-employee empid)
     (<:table 
      :border 1 :style "margin:auto;"
      (<:tr 
       (<:th "Username")(<:th "Name")(<:th "Folder")
       (<:th "Dept")(<:th "Desig")(<:th "Manager")
       (<:th "Select"))
      (loop for (id un nm fl dp ds mn) in emp-list
	 do (<:tr (<:td (<:ah un))(<:td (<:ah nm)) (<:td (<:ah fl))
		  (<:td (<:ah (dept-name-from-id dp)))
		  (<:td (<:ah (desig-name-from-id ds)))
		  (<:td (<:ah (emp-name-from-id mn)))
		  (<:td (<ucw:input :type "radio" :name "selected"
				    :accessor empid :value id))))
      (<:tr 
       (<:td :align "center" :colspan 7
	     (<:input :type "submit" :value "Edit")))))))

(defaction edit-employee (empid)
  (call-component $holder (make-instance
			   'get-employee :employee (emp-from-id empid))))

(defun next-employee-id ()
  (if (= 0 (car (clsql:select [count [*]] :from [employees] :flatp t)))
      1
      (+ 1 (car 
	    (clsql:select [max [id]] 
			  :from [employees]
			  :flatp t)))))

(defun insert-update-employee (obj)
  (if (slot-boundp obj 'id)
      (clsql:update-records-from-instance obj)
      (clsql:with-transaction ()	  
		(setf (id obj) (next-employee-id))
		(clsql:update-records-from-instance obj))))

(defun emp-from-id (emp-id);object
  (car (clsql:select 'employee   
		:where [=[slot-value 'employee 'id] emp-id]
		:flatp t)))

(defun emp-name-from-id (emp-id);name string
  (person-name-from-id 
   (car (clsql:select [slot-value 'employee 'person-id]
		      :from [employees]
		      :where [=[slot-value 'employee 'id] emp-id]))))

(defun username-from-id (id)
  (car (clsql:select
		[slot-value 'employee 'username]
		:from [employees]
		:where [= [slot-value 'employee 'id] id]
		:flatp t
		:field-names nil)))

(defun create-username (fn ln)
  (let* ((lname (string-downcase ln))
	 (fname (string-downcase fn))
	 (username (concatenate 'string lname "." fname)))
    (if (clsql:select [username]
		      :from [|employees|]
		      :limit 1
		      :flatp t
		      :field-names nil)
	username)))

(defun create-user-folder (username)
  (let* ((fsuser (car (clsql:select [fsuser] :from [companies] 
				    :where [= [id] 1] :flatp t)))
	 (user-dir (remove "." username :test #'string=))
	 (userfolder (concatenate 'string "/home/" fsuser "/users/" user-dir "/")))
    ;(ensure-directories-exist userfolder)
    userfolder))

(defun employee-tlist ();table
  (clsql:select
   [slot-value 'employee 'id]
   [slot-value 'employee 'username]
   [concat
   [slot-value 'person 'first-name] " "
   [slot-value 'person 'middle-name] " "
   [slot-value 'person 'last-name]]
   [slot-value 'employee 'folder]
   [slot-value 'employee 'dept-id]
   [slot-value 'employee 'desig-id]
   [slot-value 'employee 'manager-id]
   :from '([employees][persons])
   :where
   [=[slot-value 'employee 'person-id][slot-value 'person 'id]]
   :field-names nil
   :refresh t))

(defun manager-ddlist ();drop-down-list
  (let ((auth-des-ids ; id list of management designations
	 (clsql:select 
	  [slot-value 'designation 'id]
	  :from [|designations|]
	  :where
	  [or
	  [=[slot-value 'designation 'directoral] 1]
	  [=[slot-value 'designation 'departmental] 1]
	  [=[slot-value 'designation 'management] 1]]
	  :field-names nil
	  :flatp t)))
    (clsql:select
     [slot-value 'employee 'id] 
     [concat
     [slot-value 'person 'first-name] " "
     [slot-value 'person 'middle-name] " "
     [slot-value 'person 'last-name]]
     :from '([|employees|][|persons|])
     :where
     [and
     [in [slot-value 'employee 'desig-id] auth-des-ids]
     [= [slot-value 'employee 'person-id] [slot-value 'person 'id]]    
     ]
     :field-names nil)))

(defun emplist-from-deptid (dp-id);drop-down-list
  (clsql:select
   [slot-value 'employee 'id]
   [concat
   [slot-value 'person 'first-name] " "
   [slot-value 'person 'middle-name] " "
   [slot-value 'person 'last-name]]
   :from '([employees][persons])
   :where
   [and
   [= [slot-value 'employee 'dept-id] dp-id]
   [= [slot-value 'person 'id] [slot-value 'employee 'person-id]]
   ]
   :field-names nil))

(defun user-path-from-id (id)
  (car (clsql:select   
	[slot-value 'employee 'folder]   
	:from [employees]
	:where      
	[=[slot-value 'employee 'id] id]   
	:field-names nil
	:flatp t)))