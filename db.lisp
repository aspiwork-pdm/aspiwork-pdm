;;;; This file is part of Aspiwork Project Data Management server
;;;; Name:          db.lisp
;;;; Purpose:       defines all database classes and link classes
;;;; Programmer:    Aashish R Lokhande
;;;; Copyright (C) 2010,2011 Aashish R Lokhande
;;;; All rights reserved.
;;;;
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions
;;;; are met:
;;;; 1. Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 2. Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;; 3. Neither the name of the author nor the names of its contributors
;;;;    may be used to endorse or promote products derived from this software
;;;;    without specific prior written permission.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
;;;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
;;;; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
;;;; OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;;;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
;;;; LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
;;;; OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
;;;; SUCH DAMAGE.

(clsql:def-view-class db-obj ()
  ((id :db-kind :key :accessor id
       :db-constraints (:not-null :auto-increment)
       :type integer :initarg :id))
  (:documentation "id class, no direct mapping in db"))

;;ACCESS-LOG-------------------------------------
(clsql:def-view-class access-log ()
  ((creater-id :accessor creater-id
	       :type integer :initarg :creater-id)
   (creater :reader creater :db-kind :join
	    :db-info (:join-class employee
				  :home-key creater-id
				  :foreign-key id
				  :target-slot username
				  :set nil))
   (created-date :accessor created-date :initform (clsql:get-time)
		 :type clsql:wall-time :initarg :created-date)
   (modifier-id :type integer :initarg :modified-by)
   (modifier :reader modifier :db-kind :join
	     :db-info (:join-class employee
				   :home-key modifier-id
				   :foreign-key id
				   :target-slot username
			  :set nil))
   (modified-date :accessor modified-date :initform (clsql:get-time)
		  :type clsql:wall-time :initarg :modified-date)))

;;ADDRESS-----------------------------
(clsql:def-view-class address (db-obj)
  (;(id)
   (line1 :accessor line1 :type (clsql:varchar 50) 
	  :initarg :line1 :initform "Line 1")
   (line2 :accessor line2 :type (clsql:varchar 50) 
	  :initarg :line2 :initform "Line 2")   
   (state :accessor state :type (clsql:varchar 30) 
	  :initarg :state :initform "State")
   (pincode :accessor pincode :type (clsql:varchar 6) 
	    :initarg :pincode :initform "0")
   (phone :accessor phone :type (clsql:varchar 60) 
	  :initarg :phone :initform "0")   
   (mobile :accessor mobile :type (clsql:varchar 15) 
	   :initarg :mobile :initform "0")
   (fax :accessor fax :type (clsql:varchar 15) 
	:initarg :fax :initform "0")
   (email :accessor email :type (clsql:varchar 50) 
	  :initarg :email :initform "your@email.id")
   (website :accessor website :type (clsql:varchar 50) 
	    :initarg :website :initform "www.yourwebpage.dom"))
  (:base-table addresses))

(clsql:def-view-class address-property (db-obj)
  (;(id)
   (address-id :accessor address-id                
                :type integer
                :initarg :address-id)
   (address :accessor address
            :initarg :address
            :db-kind :join
            :db-info (:join-class address
                      :home-key address-id
                      :foreign-key id
                      :set nil)))
  (:documentation "address link class"))

;;PERSON-----------------------------
(clsql:def-view-class person (address-property)
  (;(id)
   ;(address-id)
   (title :accessor title :type (clsql:varchar 3) 
	  :initarg :title :initform "")
   (first-name :accessor first-name :initform ""
	       :type (clsql:varchar 30) :initarg :first-name)
   (middle-name :accessor middle-name :type (clsql:varchar 30)
		:initarg :middle-name :initform "")
   (last-name :accessor last-name :initform ""
	      :type (clsql:varchar 30) :initarg :last-name)
   (gender :accessor gender :type (clsql:varchar 1)
	   :initarg :gender :initform "M")
   (birth-date :accessor birth-date :initform (clsql:get-date)
	       :type clsql:date :initarg :birth-date)
   (married :accessor married :initform 0
	    :type (integer 1) :initarg :married))   
  (:base-table persons))

(clsql:def-view-class person-property (db-obj)
  (;(id)
   (person-id :accessor person-id
	      :db-constraints :not-null
	      :type integer
	      :initarg :person-id)
   (person :accessor person
            :initarg :person
            :db-kind :join
            :db-info (:join-class person
                      :home-key person-id
                      :foreign-key id
                      :set nil)))
  (:documentation "person link class"))

;;COMPANY-------------------------------
(clsql:def-view-class company (address-property access-log)
  (;(id)
   ;(address-id)
   (code :accessor code :type (clsql:varchar 6) 
	 :initarg :code :initform "")
   (name :accessor name :type (clsql:varchar 100)
	 :initarg :name :initform "")
   (extension :accessor extension :type (clsql:varchar 20) 
	      :initarg :extension :initform "")         
   (localip :accessor localip :initarg :localip 
	      :initform "0.0.0.0" :type (clsql:varchar 100))
   (fsuser :accessor fsuser :type (clsql:varchar 50) 
	   :initarg :fsuser :initform "") ;filestore pc username
   (folder  :accessor folder :type (clsql:varchar 100) 
	    :initarg :folder :initform ""))
  (:base-table companies))

;DESIGNATIONS-----------------------------
(clsql:def-view-class designation (db-obj)
  (;(id)
   (code :accessor code :type (clsql:varchar 6) 
	 :initarg :code :initform "")
   (title :accessor title :type (clsql:varchar 30) 
	  :initarg :title :initform "")   
   (directoral :accessor directoral :type (integer 1)
	       :initarg :directoral :initform 0)
   (departmental :accessor departmental :type (integer 1) 
		 :initarg :departmental :initform 0)
   (management :accessor management :type (integer 1) 
	       :initarg :management :initform 0)
   (legal :accessor legal :type (integer 1) 
	  :initarg :legal :initform 0)
   (financial :accessor financial :type (integer 1) 
	      :initarg :financial :initform 0)
   (pr :accessor pr :type (integer 1) 
       :initarg :pr :initform 0)
   (hr :accessor hr :type (integer 1) 
       :initarg :hr :initform 0)
   (expert :accessor expert :type (integer 1) 
	   :initarg :expert :initform 0)
   (operation :accessor operation :type (integer 1) 
	      :initarg :operation :initform 0)
   (general :accessor general :type (integer 1) 
	    :initarg :general :initform 0))
  (:base-table designations))

(clsql:def-view-class desig-property (db-obj)
  (;(id)
   (desig-id :accessor desig-id
	     :db-constraints :not-null
	     :type integer
	     :initarg :desig-id)
   (designation :accessor designation
		:initarg :designation
		:db-kind :join
		:db-info (:join-class designation
				      :home-key desig-id
				      :foreign-key id
				      :set nil)))
  (:documentation "designation link class"))

;DEPARTENTS------------------------------
(clsql:def-view-class department (db-obj)  
  (;(id)   
   (code :accessor code :type (clsql:varchar 6) 
	 :initarg :dept-code :initform "")
   (name :accessor name :type (clsql:varchar 30) 
	 :initarg :name :initform "")
   (head-id :accessor head-id :initform 0
	    :type integer :initarg :head-id)
   (head :reader dept-head :db-kind :join
	 :db-info (:join-class employee
			       :home-key head-id
			       :foreign-key emp-id
			       :target-slot username
			       :set nil)))
  (:base-table departments))

(clsql:def-view-class dept-property (db-obj)
  (;(id)
   (dept-id :accessor dept-id
	    :db-constraints :not-null
	    :type integer
	    :initarg :dept-id)
   (department :accessor department
	       :initarg :department
	       :db-kind :join
	       :db-info (:join-class department
			 :home-key dept-id
			 :foreign-key id
			 :set nil)))
  (:documentation "department link class"))

;EMPLOYEE-------------------------------
(clsql:def-view-class employee (person-property 
				dept-property 
				desig-property 
				access-log)
  (;(id)
   ;(person-id)  
   ;(address-id)   
   ;(dept-id)
   ;(desig-id)
   (code :accessor code :initform ""
	 :type (clsql:varchar 6) :initarg :code)   
   (username :accessor username :initform ""
	     :db-constraints :unique
	     :type (clsql:varchar 30) :initarg :username)
   (password :accessor password :initform ""
	     :type (clsql:varchar 20) :initarg :password)
   (localip :accessor localip :initarg :localip 
	      :initform "0.0.0.0" :type (clsql:varchar 100))
   (pcuser :accessor pcuser :type (clsql:varchar 50) 
	   :initarg :pcuser :initform "")
   (pcpwd :accessor pcpwd :type (clsql:varchar 50) 
	   :initarg :pcpwd :initform "")
   (pcshare :accessor pcshare :type (clsql:varchar 100) 
	   :initarg :pcshare :initform "")
   (folder :accessor folder :type (clsql:varchar 100) 
	   :initarg :folder :initform "")
   (active :accessor active :type (integer 1) 
	   :initarg :active :initform 0)
   (last-login :accessor last-login :type clsql:wall-time
	       :initarg :last-login :initform (clsql:get-time))
   (last-logout :accessor last-logout :type clsql:wall-time
		:initarg :last-logout :initform (clsql:get-time))
   (level :accessor level :initform 0
	  :type integer :initarg :level)
   (join-date :accessor join-date :type clsql:date 
	      :initarg join-date :initform (clsql:get-date))
   (manager-id :accessor manager-id
	       :type integer :initarg :manager-id)
   (qualification :accessor qualification :type (clsql:varchar 20) 
		  :initarg :qualification :initform "")
   (experience :accessor experience :type integer
	       :initarg :experience :initform 0)
   (expertise :accessor expertise :type (clsql:varchar 100) 
	      :initarg :expertise :initform "")
   (info :accessor info :type (clsql:varchar 200) 
	 :initarg :info :initform "")
   (emp-manager :accessor emp-manager :db-kind :join
		:db-info (:join-class employee
				      :home-key manager-id
				      :foreign-key id
				      :target-slot username
				      :set nil))
   (emp-dept :accessor emp-dept :db-kind :join
	     :db-info (:join-class department
				   :home-key dept-id
				   :foreign-key id
				   :target-slot name
				   :set nil))
   (emp-desig :accessor emp-desig :db-kind :join
	      :db-info (:join-class designation
				    :home-key desig-id
				    :foreign-key id
				    :target-slot title
				    :set nil)))
  (:base-table employees))

(clsql:def-view-class employee-property (db-obj)
  (;(id)
   (employee-id :accessor employee-id	      
		:type integer
		:initarg :employee-id)
   (employee :accessor employee
	     :initarg :employee
	     :db-kind :join
	     :db-info (:join-class employee
				   :home-key employee-id
				   :foreign-key id
				   :set nil)))
  (:documentation "employee link class"))

;LOGINS--------------------------------
(clsql:def-view-class current-logins (employee-property)
  ((status :accessor status :initarg :status 
		   :type (integer 1) :initform 0)
   (session-id :accessor session-id :initarg :session-id
			   :type integer :initform 0))
  (:base-table current-logins))

;CUSTOMERS--------------------------------
(clsql:def-view-class customer (address-property access-log)
  (;(id)
   ;(address-id)
   (code :accessor code :initform ""
	 :type (clsql:varchar 6) :initarg :code)
   (name  :accessor name :type (clsql:varchar 100) 
	  :initarg :name :initform "")      
   (info :accessor info :type (clsql:varchar 200) 
	 :initarg :info :initform ""))
  (:base-table customers))

(clsql:def-view-class customer-property (db-obj)
  (;(id)
   (customer-id :accessor customer-id	      
		:type integer
		:initarg :customer-id)
   (customer :accessor customer
	     :initarg :customer
	     :db-kind :join
	     :db-info (:join-class customer
				   :home-key customer-id
				   :foreign-key id
				   :set nil)))
  (:documentation "customer link class"))

;AGENCY--------------------------------
(clsql:def-view-class agency (address-property access-log)
  (;(id)
   ;(address-id)
   (code :accessor code :initform ""
	 :type (clsql:varchar 6) :initarg :code)
   (name  :accessor name :type (clsql:varchar 100) 
	  :initarg :name :initform "")      
   (info :accessor info :type (clsql:varchar 200) 
	 :initarg :info :initform ""))
  (:base-table agencies))

(clsql:def-view-class agency-property (db-obj)
  (;(id)
   (agency-id :accessor agency-id	      
		:type integer
		:initarg :agency-id)
   (agency :accessor agency
	     :initarg :agency
	     :db-kind :join
	     :db-info (:join-class agency
				   :home-key agency-id
				   :foreign-key id
				   :set nil)))
  (:documentation "agency link class"))

;PROJECTS---------------------------------
(clsql:def-view-class project (address-property 
			       dept-property 
			       customer-property 			       
			       access-log)
  (;(id)
   ;(addess-id)
   ;(dept-id)
   ;(customer-id)
   (code :accessor code :initform ""
	 :type (clsql:varchar 6) :initarg :code)
   (name :accessor name :type (clsql:varchar 50) 
	 :initarg :name :initform "")
   (manager-id :accessor manager-id
	       :type integer :initarg :manager-id)
   (active :accessor active :initform 0
	   :type (integer 1) :initarg :active)
   (start-date :accessor start-date :type clsql:date 
	       :initarg :start-date :initform (clsql:get-date))
   (finish-date :accessor finish-date :type clsql:date 
		:initarg :finish-date :initform (clsql:get-date))
   (info :accessor info :type (clsql:varchar 200) 
	 :initarg :info :initform "")
   (prj-manager  :reader prj-manager :db-kind :join
		 :db-info (:join-class employee
				       :home-key manager-id
				       :foreign-key id
				       :target-slot username
				       :set nil))
   (prj-dept :reader prj-dept :db-kind :join
	     :db-info (:join-class department
				   :home-key dept-id
				   :foreign-key id
				   :target-slot name
				   :set nil))
   (prj-team :accessor prj-team :db-kind :join
	     :db-info (:join-class team
				   :home-key id
				   :foreign-key project-id
				   :set t))
   (prj-sections :accessor prj-sections :db-kind :join
		 :db-info (:join-class project-section
				       :home-key id
				       :foreign-key project-id
				       :set t))
   (prj-customer :accessor prj-customer :db-kind :join
		 :db-info (:join-class customer
				       :home-key customer-id
				       :foreign-key id
				       :target-slot name
				       :set nil)))
  (:base-table projects))

(clsql:def-view-class project-property (db-obj)
  (;(id)
   (project-id :accessor project-id	    
	       :type integer
	       :initarg :project-id)
   (project :accessor project
	    :initarg :project
	    :db-kind :join
	    :db-info (:join-class project
				  :home-key project-id
				  :foreign-key id
				  :set nil)))
  (:documentation "project link class"))

;TEAMS--------------------------------------------
(clsql:def-view-class team (project-property)
  (;(id)
   ;(project-id)
   (member-id :accessor member-id
	      :type integer :initarg member-id)
   (team-member :accessor team-member :db-kind :join
		:db-info (:join-class employee
				      :home-key member-id
				      :foreign-key id
				      :set nil)))   
  (:base-table teams))

;PROJECT SECTIONS----------------------------------
(clsql:def-view-class project-section (project-property)
  (;(id)
   ;(project-id)
   (code :accessor code :initform ""
	 :type (clsql:varchar 6) :initarg :code)
   (name :accessor name :initform ""
	 :type (clsql:varchar 30) :initarg :name)
   (info :accessor info :type (clsql:varchar 200) 
	 :initarg :info :initform ""))
  (:base-table project-sections))

(clsql:def-view-class section-property (db-obj)
  (;(id)
   (section-id :accessor section-id	   
	       :type integer
	       :initarg :section-id)
   (prj-section :accessor prj--section
		    :initarg :project-section
		    :db-kind :join
		    :db-info (:join-class project-section
					  :home-key section-id
					  :foreign-key id
					  :set nil)))
  (:documentation "project-section link class"))

;PROJECT AGENCIES----------------------------------
(clsql:def-view-class project-agency (project-property 
				      agency-property)
  (;(id)
   ;(project-id)
   ;(agency-id) 
   )
  (:base-table project-agencies))

;CONTACTS---------------------------------
(clsql:def-view-class contact-person (person-property 
				      project-property
				      customer-property
				      agency-property
				      access-log)
  (;(id)
   ;(person-id)
   ;(project-id)
   ;(customer-id)
   ;(agency-id)
   (designation :accessor designation :initform ""
		:type (clsql:varchar 30) :initarg :designation)
   (info :accessor info :type (clsql:varchar 100) 
	 :initarg :info :initform ""))
  (:base-table contact-persons))

;FILE CATEGORIES------------------------------------
(clsql:def-view-class resource-category (dept-property )
  (;(id)
   ;(dept-id)
   (code :accessor code :initform ""
	 :type (clsql:varchar 6) :initarg :code)
   (name :accessor name :initform ""
	 :type (clsql:varchar 30) :initarg :name))   
  (:base-table resource-categories))

(clsql:def-view-class category-property (db-obj)
  (;(id)
   (category-id :accessor category-id
		:db-constraints :not-null
		:type integer
		:initarg :category-id)
   (resource-category :accessor resource-category
		      :initarg :resource-category
		      :db-kind :join
		      :db-info (:join-class resource-category
					    :home-key category-id
					    :foreign-key id
					    :set nil)))
  (:documentation "resource-category link class"))

;FILE TYPES------------------------------------
(clsql:def-view-class file-type (db-obj)
  (;(id)
   (name :accessor name :initform ""
	 :type (clsql:varchar 30) :initarg :name)
   (extension :accessor extension :initform ""
	      :type (clsql:varchar 20) :initarg :extension))
  (:base-table file-types))

(clsql:def-view-class type-property (db-obj)
  (;(id)
   (type-id :accessor type-id	   
	    :type integer
	    :initarg :type-id)
   (file-type :accessor file-type
	  :initarg :file-type
	  :db-kind :join
	  :db-info (:join-class file-type
				:home-key type-id
				:foreign-key id
				:set nil)))
  (:documentation "file-type link class"))

;RESOURCES/FILES--------------------------------
(clsql:def-view-class resource (project-property 				
				section-property 
				category-property 
				type-property
				access-log)
  (;(id)
   ;project-id)
   ;category-id)
   ;type-id)
   ;section-id)
   (code :accessor code :initform ""
	 :type (clsql:varchar 10) :initarg :code)   
   (name :accessor name :type (clsql:varchar 100) 
	 :initarg :name :initform "")
   (ref-id :accessor ref-id :type integer
	   :initarg :ref-id :initform 0)
   ;first file identified with 0
   ;revisions get id of original file
   (current :accessor current :type (clsql:varchar 100) 
	 :initarg :current :initform "")
   (folder :accessor folder :type (clsql:varchar 100) 
	   :initarg :folder :initform "")
   (in-use :accessor in-use :initform 0
	   :type (integer 1) :initarg :in-use)   
   (locked :accessor locked :initform 0
	   :type (integer 1) :initarg :locked)
   (version :accessor version :type integer 
	    :initarg :version :initform 0)
   (revision :accessor revision :type integer 
	     :initarg :revision :initform 0)
   (save-count :accessor save-count :type integer
	       :initarg :save-count :initform 0)
   (info :accessor info :type (clsql:varchar 200) 
	 :initarg :info :initform "")
   (comment :accessor comment :type (clsql:varchar 200) 
	    :initarg :comment :initform "")
   (init-comment :accessor init-comment :type (clsql:varchar 200) 
		 :initarg :init-comment :initform "")
   (last-comment :accessor last-comment :type (clsql:varchar 200) 
		 :initarg :last-comment :initform ""))
  (:base-table resources))

;CURRENT-FILES-----------------------------
(clsql:def-view-class current-file ()
  (;(id)
   (resource-id :accessor resource-id :initform 0
		:initarg :resource-id :type integer)
   (accessor-id :accessor accessor-id
		:type integer :initarg :accessor-id)
   (accessor :reader accessor :db-kind :join
	     :db-info (:join-class employee
				   :home-key accessor-id
				   :foreign-key id
				   :target-slot username
				   :set nil))
   (accessed-date :accessor accessed-date :initform (clsql:get-time)
		  :type clsql:wall-time :initarg :accessed-date)
   (central-location :accessor central-location :initform ""
		     :initarg :central-location :type (clsql:varchar 200))
   (fetch-location :accessor fetch-location :initform ""
		   :initarg :fetch-location :type (clsql:varchar 200)))
  (:base-table current-files))

;PERMISSIONS------------------------------
(clsql:def-view-class permission (desig-property 
				  category-property )
  (;(id)   
   ;(desig-id)
   ;(category-id)
   )
  (:base-table permissions))


